
import React, { FunctionComponent, useEffect, useState } from 'react';
import { Input, Select, Row, Col } from 'antd';
// @ts-ignore
import styles from './index.module.less';

const { Option } = Select;
interface IProps {
    terminals: any;
    defTerminals?: string;
    onChange?: Function;
}

const DevicePanel: FunctionComponent<IProps> = props => {
    const terminalSize = Object.keys(props.terminals).length - 2;
    const [terminal, setTerminal] = useState<any>(props.terminals[Object.keys(props.terminals)[terminalSize]]);

    const onChange = (value: any) => {
        const item = props.terminals[value];
        setTerminal(item);
        if (props.onChange)
            props.onChange(value)
    }

    return (
        <Row gutter={[16, 16]}>
            <Col span={24}>
                <Select
                    defaultValue={props.defTerminals ? props.defTerminals : Object.keys(props.terminals)[terminalSize]}
                    showSearch
                    onChange={onChange}
                    optionFilterProp="children"
                    size="small"
                    className={styles.device_panel}
                    style={{ width: '160px' }}
                >
                    {
                        Object.keys(props.terminals).map((key) => {
                            return <Option key={key} value={key}>{key}</Option>
                        })
                    }
                </Select>
                <Input className={styles.device_panel} style={{ width: 60 }} size="small" value={terminal.w} />
                <span className={styles.device_panel}>×</span>
                <Input className={styles.device_panel} style={{ width: 60 }} size="small" value={terminal.h} />
                {props.children || null}
            </Col>
        </Row>
    )
}

export { DevicePanel }