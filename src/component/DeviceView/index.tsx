
import React, { FunctionComponent, ReactElement, useEffect, useState } from 'react';
import { Icon, Tooltip } from 'antd';
import QrCode from 'qrcode.react'
import { DevicePanel } from '../DevicePanel/index';
// @ts-ignore
import styles from './index.module.less';
// import img_stateBar from './stateBar.svg';
const img_stateBar = require('./stateBar.svg')

const terminals: any = {
    'Pixel 2': {
        w: 411,
        h: 731
    },
    'Pixel 2 XL': {
        w: 411,
        h: 823
    },
    'iPhone 5/SE': {
        w: 320,
        h: 568
    },
    'iPhone 6/7/8': {
        w: 375,
        h: 667
    },
    'iPhone 6/7/8 Plus': {
        w: 414,
        h: 736
    },
    'iPhone X': {
        w: 375,
        h: 812
    }
};

interface terminalsProps {
    /** width pixel */
    w: number
    /** height pixel */
    h: number
}

interface IProps {
    src: string
    children?: ReactElement
    terminals?: Array<terminalsProps>
    defTerminals?: string
    style?: React.CSSProperties
    padding?: string
    background?: string
}

const DeviceView: FunctionComponent<IProps> = props => {
    const { padding = '16px', background = '#fff' } = props;
    const terminalSize = Object.keys(props.terminals || terminals).length - 1;
    const [refresh, setRefresh] = useState(false);
    const [terminal, setTerminal] = useState<any>(terminals[Object.keys(props.terminals || terminals)[terminalSize]]);
    const [currentDrag, setCurrentDrag] = useState<number | null>(null);
    const [imgHeight, setImgHeight] = useState<number>(0);
    const [iframeStyle, setIframeStyle] = useState<React.CSSProperties>({ width: '100%', border: 'none', display: 'block' })
    let myStateBar: any;

    useEffect(() => {
        const preview = getUrlParame('preview', window.location.search);
        if (preview === 'mobile') {
            setIframeStyle({
                ...iframeStyle, ...{
                    top: 0,
                    left: 0,
                    height: '100%',
                    position: 'fixed',
                    zIndex: 1000
                }
            })
        }
        if (props.defTerminals) {
            const item = terminals[props.defTerminals];
            setTerminal(item);
        }
    }, [])

    const onChange = (value: any) => {
        const item = terminals[value];
        setTerminal(item);
    };

    const listen = () => {
        try {
            const iframe: any = window.document.getElementById("iframeDeviceView");
            const head = iframe.contentWindow.document.getElementsByTagName('head')[0];
            head.innerHTML += `<style type="text/css">
            html {
                padding: ${padding};
                background: ${background};
                user-select: none;
            }
            html::-webkit-scrollbar {
                width: 0;
            }
            html::-webkit-scrollbar-thumb {
                border-radius: 10px;
                background: rgba(128, 134, 149, 0.5);
            }
            html::-webkit-scrollbar-track {
                border-radius: 10px;
                background: rgba(232, 234, 236, 1);
            }
            </style>`
            const addClassStyle = (className: string, css: string) => {
                head.innerHTML += `<style type="text/css">${className}{
                    ${css}
                }</style>`;
            }
            const root = iframe.contentWindow.document.getElementById('root');
            const docPage = root.childNodes[0].childNodes[0].childNodes[0].childNodes[0];
            const canvas = root.childNodes[0].childNodes[0].childNodes[0];

            addClassStyle('.' + canvas.className.split("\ ")[0], 'background: transparent; ');
            addClassStyle('.' + docPage.className.split("\ ")[0], 'padding: 0; width: 100% !important;');
            addClassStyle('body', 'background: inherit;');
        } catch (e) {

        }
    }

    const getUrlParame = (name: string, str: string) => {
        const reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`);
        const r = str.substr(1).match(reg);
        if (r != null) return decodeURIComponent(r[2]); return null;
    }

    return (
        <div style={props.style}>
            <DevicePanel defTerminals={props.defTerminals} terminals={terminals} onChange={onChange}>
                <Tooltip placement="rightBottom" title={<QrCode value={
                    window.location.href + '?preview=mobile&t=' + new Date().getTime()
                } size={200} />} arrowPointAtCenter>
                    <Icon style={{ fontSize: '1.5rem', marginTop: 10 }} type="qrcode" />
                </Tooltip>
            </DevicePanel>
            <div style={{ width: terminal.w, minHeight: terminal.h }} className={styles.drag_drop}>
                <div className={styles.state_bar}>
                    <img ref={myStateBar} onLoad={e => {
                        if (myStateBar)
                            setImgHeight(myStateBar.current.height)
                    }
                    } src={img_stateBar} />
                    <iframe id="iframeDeviceView" onLoad={listen} style={{ ...{ height: (terminal.h - imgHeight), ...iframeStyle } }} src={props.src}></iframe>
                </div>
            </div>
        </div>
    )
}

export { DeviceView }