#!/usr/bin/env node

// 使用严格模式
'use strict';

const fs = require('fs');
const readFileSync = fs.readFileSync;
const writeFileSync = fs.writeFileSync;
const path = require('path');
const prettier = require('prettier');
const chalk = require('chalk');
const { execSync } = require('child_process');

const packageJSONFilePath =  path.join(process.cwd(), 'package.json');
if (!fs.existsSync(packageJSONFilePath)) {
  console.log(chalk.red('找不到 package.json 文件，请检查'));
  process.exit(1);
}

const packageJsonFile = readFileSync(packageJSONFilePath, 'utf-8');

let packageJsonObj = {};
try {
  packageJsonObj = JSON.parse(packageJsonFile);
}catch (e) {
  console.log(chalk.red('package.json 格式错误，请检查'));
  process.exit(1);
}

if (!packageJsonObj) {
  return;
}

let version = packageJsonObj['version'];

const versions = version.split('.');
const lastVersions = versions[versions.length - 1];
versions.pop();
versions.push((Number(lastVersions) + 1).toString());
const newVersion = versions.join('.');

packageJsonObj['version'] = newVersion;

const prettierCode = prettier.format(JSON.stringify(packageJsonObj), {
  tabWidth: 2,
  semi: false,
  singleQuote: true,
  parser: 'json',
});

writeFileSync(packageJSONFilePath, prettierCode, 'utf-8');

execSync('git add package.json');

process.exit(0);
