#!/usr/bin/env node

// 使用严格模式
'use strict';

const fs = require('fs');
const path = require('path');
const readFileSync = fs.readFileSync;
const writeFileSync = fs.writeFileSync;

const prettier = require('prettier');

const cwd = process.cwd();
const versionObj = {};
const format = 'utf-8';

const rootPackageJsonFilePath =  path.join(cwd, 'package.json');
const rootPackageJsonObj = JSON.parse(readFileSync(rootPackageJsonFilePath, format));
versionObj['wren-ui'] = rootPackageJsonObj['version'];

const packagesDirPath = path.join(cwd, 'packages');

const packages = fs.readdirSync(packagesDirPath);
packages.forEach(packageDir => {
  const packagePath = path.join(packagesDirPath, packageDir);
  const stat = fs.statSync(packagePath);
  if (stat.isDirectory()) {
    const packageJsonPath = path.join(packagePath, 'package.json');
    if (!fs.existsSync(packageJsonPath)) {
      return;
    }

    const obj = JSON.parse(readFileSync(packageJsonPath, format));
    versionObj[obj['name']] = obj['version'];
  }
});

const result = `
export default ${JSON.stringify(versionObj)}`;

const prettierCode = prettier.format(result, {
  singleQuote: true,
  trailingComma: 'es5',
  printWidth: 100,
  parser: 'typescript',
});

const writePath = path.join(cwd, 'version.ts');
writeFileSync(writePath, prettierCode, 'utf-8');

console.log('版本写入成功');

process.exit();


