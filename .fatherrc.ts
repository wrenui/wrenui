import { IBundleOptions } from 'father-build/src/types';
import { Config as IDoczConfig } from 'docz-core';

interface IOptions extends IBundleOptions {
  doc?: Partial<IDoczConfig>;
}

const { ENV_TYPE } = process.env;

const options: IOptions = {
  cjs: 'rollup',
  esm: 'rollup',
  doc: {
    ignore: ['*.md', '*/**/*.md'],
    // src: 'packages/',
    typescript: true,
    themeConfig: {
      // codemirrorTheme: 'docz-dark',
      // codemirrorTheme: 'dracula',
    }
  },
  pkgs: [
    'basis',
    'feedback',
    'showoff',
    'profession'
  ],
  // 按需加载
  extraBabelPlugins: [
    // antd
    ['babel-plugin-import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: 'css',
    }],
    // antd-mobile
    ['babel-plugin-import', {
      libraryName: 'antd-mobile',
      libraryDirectory: 'es',
      style: 'css',
    }, 'antd-mobile'],
    ['babel-plugin-import', {
      libraryName: '@wren-ui/mobile',
      libraryDirectory: 'es',
      style: true,
    }, '@wren-ui/mobile']
  ]
};

// 在打包的时候引入
if (ENV_TYPE === 'doc:build' || ENV_TYPE === 'doc:dev') {
  options.doc.htmlContext = {
    // @ts-ignore
    head: {
      links: [
        // { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/antd/3.26.14/antd.min.css' },
        // { rel: 'stylesheet', href: 'https://unpkg.com/antd-mobile@2.3.3/dist/antd-mobile.min.css' },
        // { rel: 'stylesheet', href: 'https://codemirror.net/theme/dracula.css' },
      ],
      raw: `
      <style type="text/css">
        html::-webkit-scrollbar {
          width: 6px;
        }

        html::-webkit-scrollbar-thumb {
          border-radius: 10px;
          background: rgba(128, 134, 149, 0.5);
        }

        html::-webkit-scrollbar-track {
          border-radius: 10px;
          background: rgba(232, 234, 236, 1);
        }

        .kQMXGY::-webkit-scrollbar {
          width: 6px;
        }

        .kQMXGY::-webkit-scrollbar-thumb {
          border-radius: 10px;
          background: rgba(128, 134, 149, 0.5);
        }

        .kQMXGY::-webkit-scrollbar-track {
          border-radius: 10px;
          background: rgba(232, 234, 236, 1);
        }

        div[class^='CustomMenu__ItemTitle-'] {font-weight: 700;}
        div[class^='CustomMenu__Item-'] {text-indent: 1em; white-space: nowrap;}
      </style>
      `
    },
  }
}

export default options;
