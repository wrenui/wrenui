# wren-ui

## 项目结构：

```
 │
 ├─ packages                    // npm 包目录
 │  ├─ auth                     // 授权、token 相关
 │  ├─ component                // 通用组件包
 │  ├─ http                     // http 请求工具
 │  ├─ mobile-eco               // 移动端工具、微信授权等业务
 │  ├─ node-util                // node 专用工具
 │  ├─ util                     // 通用工具类
 ├── .fatherrc.ts               // doc 配置
 ├── .gitignore
 ├── package-lock.json
 └── package.json
```

## 安装依赖
    yarn

## 安装 packages 里面的依赖（可选）
    npm run bootstrap

## 编译

全局编译: 在项目根目录下执行 
```
npm run build:pro
```

单独编译: 在每个包下创建 .fatherrc.ts 文件，并且在包下在执行 
```
npm run build
```

## 创建包
（通常情况下仅需要在已经建立的包内进行开发即可）

1. 生成
`lerna create [name]`，如果提示：`lerna : 无法将“lerna”项识别为 cmdlet、函数、脚本文件或可运行程序的名称。请检查名称的拼写，如果包括路径，请确保路径正确，然后再试一次。`，请全局安装 lerna (`yarn global add lerna`)

2. 添加其他必要文件到包下，从其它包中拷一份即可
tsconfig.json、typings.d.ts、public_api.d.ts

3. 修改 package.json 中下方字段值
    ```json
    {
        ...
        "name": "@wren-ui/xxx",
        "main": "dist/index.js",
        "module": "dist/index.esm.js",
        "files": [
            "dist",
            "public_api.d.ts"
        ],
        ...
    }
    ```

4. 创建 src 目录，以及该目录下的 index.ts 文件

5. 在项目根目录下的 .fatherrc.ts 中的 pkgs 增加新加的包名


## 调试
```
1.在指定包目录下运行
    npm link

2.在要调试的项目下运行
    npm link @wren-ui/包名
```

## 包管理
```
添加模块到包内
    lerna add axios --scope=@wren-ui/http

添加包的相互依赖
    lerna add @wren-ui/http --scope=@wren-ui/auth
```


## 发布
进入每个包运行发布命令
```
    npm publish -access public
```

## 用户名密码

先切换到npm官方源：

    npm config set registry https://registry.npmjs.org/

发布完成后记得切回内网源头

```
//公网：
npm config set registry http://npm.vgogbuy.cn
yarn config set registry http://npm.vgogbuy.cn

//内网：
npm config set registry http://10.10.11.102:4873
yarn config set registry http://10.10.11.102:4873
```

登录：

    npm login

// 账号：guccihuiyuan

// 密码：sola-roxas

// Email：704807396@qq.com


## 取消发布
```
npm unpublish @wren-ui/component@0.5.0
```

## 文档

1. 创建文档
    创建 `.mdx` 文件，文件内容开头如下：
    ```
    ---
    name: FormGroup
    route: /component/FormGroup
    menu: component
    ---
    ```

2. 启动预览文档
    ```
    npm run doc:dev // 如果提示 “Failed to compile.” 需要先执行 npm run build:dev
    ```

### 编写规范，参照 [Docz](https://www.docz.site/)

### 内容规范：
```
---
name: Example 示例
route: /example
menu: wren-ui/mobile
---

# FormGroup 表单组

简单描述

## 何时使用

## 演示

### 组件

## API
### Props

## 使用

### 代码示例

```


### 组件开发规范

```
/component-name
    /__test__
    /demos
    /style
        index.less
        index.tsx
    index.mdx
    index.tsx
```

* 组件目录小写，多个单词用减号（-）分隔
* 组件测试用例放入 `/__test__` 目录
* 组件示例放入 `/demos` 目录，这样不会被打包
* 组件样式放入 `/style` 目录，该目录下必须有 `index.(tsx/ts/jsx)`，在该文件中导入 less 文件;

