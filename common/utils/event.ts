export function stopPropagation(event: React.TouchEvent<HTMLDivElement>) {
  event.stopPropagation();
}

export function preventDefault(event: React.TouchEvent<HTMLDivElement>, isStopPropagation?: boolean) {
  /* istanbul ignore else */
  if (typeof event.cancelable !== 'boolean' || event.cancelable) {
    event.preventDefault();
  }

  if (isStopPropagation) {
    stopPropagation(event);
  }
}