import { IIconProps } from '../../packages/basis/src/icon/demos/params';

/**
 * 节流
 * @param callback
 * @param delay
 */
export function throttle(callback: (...rest: any[]) => void, delay: number) {
  let previous = 0;
  // tslint:disable-next-line: only-arrow-functions
  return function(...args: any[]) {
    const now = +new Date();
    if (now - previous > delay) {
      callback.call(null, ...args);
      previous = now;
    }
  };
}

export const inBrowser = typeof window !== 'undefined';

// 判断是否为空
export function isDef<T>(val: T): val is NonNullable<T> {
  return val !== undefined && val !== null;
}
// 判断是否数值型
export function isNumeric(val: string | number): val is string {
  return typeof val === 'number' || /^\d+(\.\d+)?$/.test(val);
}
// 判断是否图片地址
export function isImage(name?: string) {
  return name ? name.indexOf('/') !== -1 : false;
}
// 新增px单位
export function addUnit(value?: string | number): string | undefined {
  if (!isDef(value)) {
    return undefined;
  }
  return isNumeric(value) ? `${value}px` : String(value);
}
// 拷贝到设备
export function copyToClipboard(str: string) {
  const el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);

  const selected =
    document.getSelection().rangeCount > 0
      ? document.getSelection().getRangeAt(0)
      : false;

  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);

  if (selected) {
    document.getSelection().removeAllRanges();
    document.getSelection().addRange(selected);
  }
}
// 点击拷贝图标
export function copyIcon(icon: string, option: IIconProps = {}) {
  let tag = `<Icon name="${icon}"`;
  if ('className' in option) {
    tag = `${tag} className={${option.className}}`;
  }
  if ('dot' in option) {
    tag = `${tag} dot`;
  }
  if ('badge' in option) {
    tag = `${tag} badge={${option.badge}}`;
  }
  if ('content' in option) {
    tag = `${tag} content={${option.content}}`;
  }
  if ('color' in option) {
    tag = `${tag} color={${option.color}}`;
  }
  if ('size' in option) {
    tag = `${tag} size={${option.size}}`;
  }
  tag = `${tag} />`;
  copyToClipboard(tag);
  return tag;
}
// 点击拷贝颜色
export function copyColor(color: string) {
  copyToClipboard(color);
  return color;
}