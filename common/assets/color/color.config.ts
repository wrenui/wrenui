export default {
  name: 'color',
  colorList: [
    {
      // 主色
      name: 'main',
      title: '主色',
      description: 'Mobile UI 使用蓝色作为主色调，其中 Light Primary 常用于 hover，Dark Primary 常用于 active。',
      list: [
        {
          name: 'Primary',
          color: '#2d8cf0'
        },
        {
          name: 'Light Primary',
          color: '#5cadff'
        },
        {
          name: 'Dark Primary',
          color: '#2b85e4'
        },
      ]
    }, {
      // 辅助色
      name: 'auxiliary',
      title: '辅助色',
      description: '辅助色是具有代表性的颜色，常用于信息提示，比如成功、警告和失败。',
      list: [
        {
          name: 'Info',
          color: '#2db7f5'
        },
        {
          name: 'Success',
          color: '#19be6b'
        },
        {
          name: 'Warning',
          color: '#ff9900'
        },
        {
          name: 'Error',
          color: '#ed4014'
        },
      ]
    }, {
      // 中性色
      name: 'neutral',
      title: '中性色',
      description: '中性色常用于文本、背景、边框、阴影等，可以体现出页面的层次结构。',
      list: [
        {
          name: '标题 Title',
          color: '#17233d'
        },
        {
          name: '正文 Content',
          color: '#515a6e'
        },
        {
          name: '辅助/图标 Sub Color',
          color: '#808695'
        },
        {
          name: '失效 Disabled',
          color: '#c5c8ce'
        },
        {
          name: '边框 Border',
          color: '#dcdee2'
        },
        {
          name: '分割线 Divider',
          color: '#e8eaec'
        },
        {
          name: '背景 Background',
          color: '#f8f8f9'
        },
      ]
    }
  ]
};
