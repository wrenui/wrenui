import React, { CSSProperties, FunctionComponent } from 'react';
import './style';
import { CSSTransition } from 'react-transition-group';
import { Icon, Overlay } from '../../../../index';
import { useLockScroll } from '../../../../common/utils/use-lock-scroll';
interface IProps {
  // 弹出层类名
  className?: string;
  // 样式内容
  style?: CSSProperties;
  // 显示Popup控制
  show: boolean;
  // 显示遮罩层控制
  overlayShow?: boolean;
  // 最外层父级层级
  zIndex?: number | string;
  // 过渡动画类名(仅在postion为center时生效)
  transition?: string;
  // 遮罩层类名
  overlayClassName?: string;
  // 遮罩层样式内容
  overlayStyle?: CSSProperties;
  // 弹出层相对位置
  postion?: 'center' | 'bottom' | 'top' | 'left' | 'right';
  // 点击遮罩层是否可关闭面板
  closeOnClickOverlay?: boolean;
  // 锁定遮罩层下背景滚动
  isLockScroll?: boolean;
  // 是否显示圆角
  round?: boolean;
  // 是否显示关闭按钮
  closeable?: boolean;
  // 关闭按钮位置
  closeIconPosition?: 'top-right' | 'top-left' | 'bottom-right' | 'bottom-left';
  // 自定义关闭按钮
  closeIcon?: string;
  // 自定义关闭按钮大小
  closeIconSize?: number | string;
  // 弹出层点击事件
  onClick?: () => void;
  // 点击遮罩层事件
  onClose?: () => void;
}

const Popup: FunctionComponent<IProps> = (props) => {
  const {
    show = false,
    overlayShow = true,
    zIndex = 2000,
    className = '',
    style = {},
    transition = 'fade',
    overlayClassName = '',
    overlayStyle = {},
    postion = 'center',
    closeOnClickOverlay = true,
    isLockScroll = true,
    round,
    closeable,
    closeIconPosition = 'top-right',
    closeIcon,
    closeIconSize = 22,
    onClick = () => { },
    onClose = () => { }
  } = props;
  const [lockScroll, unlockScroll] = useLockScroll(() => isLockScroll);
  const styleSelf: CSSProperties = {
    zIndex: zIndex !== undefined ? +zIndex + 1 : undefined,
    ...style,
  };

  // 获取关闭按钮
  const renderCloseIcon = () => {
    if (closeable) {
      return <Icon
        name={closeIcon ? closeIcon : 'cross'}
        size={closeIconSize}
        color="#c8c9cc"
        className={['close_icon', `close_icon_${closeIconPosition}`].filter((v) => v).join(' ')}
        style={zIndex ? { zIndex: Number(`${+zIndex + 1}`) } : { zIndex: 1 }}
        onClick={() => { onClose(); }}
      />;
    }
    return;
  };
  return (
    <>
      <Overlay className={overlayClassName} style={overlayStyle} show={show && overlayShow} zIndex={zIndex} onClick={() => { closeOnClickOverlay ? onClose() : null; }}></Overlay>
      <CSSTransition
        in={show}
        classNames={postion === 'center' ? transition : `popup_${postion}`}
        timeout={300}
        unmountOnExit
        appear={true}
        onEntered={() => { lockScroll(); }}
        onExited={() => { unlockScroll(); }}
      >
        <div
          className={['popuo_wrapper', `popuo_${postion}`, round ? `popup_${postion}_round` : '', className].filter((v) => v).join(' ')}
          style={styleSelf}
          onClick={() => { onClick(); }}
        >
          {props?.children}
          {/* 关闭按钮 */}
          {renderCloseIcon()}
        </div>
      </CSSTransition>
    </>
  );
};

export default Popup;