export enum EPopupType {
  basis = 'basis',
  top = 'top',
  bottom = 'bottom',
  left = 'left',
  right = 'right',
  closeIcon = 'closeIcon',
  customizeIcon = 'customizeIcon',
  postionIcon = 'postionIcon',
  roundBottom = 'roundBottom',
  roundTop = 'roundTop',
  roundLeft = 'roundLeft',
  roundRight = 'roundRight',
}