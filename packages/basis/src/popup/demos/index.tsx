import React, { FunctionComponent, useState } from 'react';
// import styles from './demo.module.less';
import { Cell, CellGroup, Popup } from '../../../../../index';
import { EPopupType } from './params';
interface IPrpos {

}

const PopupDemo: FunctionComponent<IPrpos> = (props) => {
  // 显示基础用法弹出层
  const [basisPopup, setBasisPopup] = useState<boolean>(false);
  // 显示顶部弹出弹出层
  const [topPopup, setTopPopup] = useState<boolean>(false);
  // 显示底部弹出弹出层
  const [bottomPopup, setBottomPopup] = useState<boolean>(false);
  // 显示左侧弹出弹出层
  const [leftPopup, setLeftPopup] = useState<boolean>(false);
  // 显示右侧弹出弹出层
  const [rightPopup, setRightPopup] = useState<boolean>(false);
  // 显示显示关闭按钮弹出层
  const [closeIconPopup, setCloseIconPopup] = useState<boolean>(false);
  // 显示显示自定义关闭按钮弹出层
  const [customizeIconPopup, setCustomizeIconPopup] = useState<boolean>(false);
  // 显示控制图标位置弹出层
  const [postionIconPopup, setPostionIconPopup] = useState<boolean>(false);
  // 显示底部圆角弹出层
  const [roundBottomPopup, setRoundBottomPopup] = useState<boolean>(false);
  // 显示顶部圆角弹出层
  const [roundTopPopup, setRoundTopPopup] = useState<boolean>(false);
  // 显示左侧圆角弹出层
  const [roundLeftPopup, setRoundLeftPopup] = useState<boolean>(false);
  // 显示右侧圆角弹出层
  const [roundRightPopup, setRoundRightPopup] = useState<boolean>(false);
  // 点击遮罩层，关闭弹出层
  const onClosePopup = (popupType: string) => {
    switch (popupType) {
      default:
      case EPopupType.basis:
        setBasisPopup(false);
        break;
      case EPopupType.top:
        setTopPopup(false);
        break;
      case EPopupType.bottom:
        setBottomPopup(false);
        break;
      case EPopupType.left:
        setLeftPopup(false);
        break;
      case EPopupType.right:
        setRightPopup(false);
        break;
      case EPopupType.closeIcon:
        setCloseIconPopup(false);
        break;
      case EPopupType.customizeIcon:
        setCustomizeIconPopup(false);
        break;
      case EPopupType.postionIcon:
        setPostionIconPopup(false);
        break;
      case EPopupType.roundBottom:
        setRoundBottomPopup(false);
        break;
      case EPopupType.roundTop:
        setRoundTopPopup(false);
        break;
      case EPopupType.roundLeft:
        setRoundLeftPopup(false);
        break;
      case EPopupType.roundRight:
        setRoundRightPopup(false);
        break;
    }
  };
  return (
    <>
      <CellGroup title="基础用法">
        <Cell arrow onClick={() => { setBasisPopup(true); }}>展示弹出层</Cell>
      </CellGroup>
      <CellGroup title="弹出位置">
        <Cell arrow onClick={() => { setTopPopup(true); }}>顶部弹出</Cell>
        <Cell arrow onClick={() => { setBottomPopup(true); }}>底部弹出</Cell>
        <Cell arrow onClick={() => { setLeftPopup(true); }}>左侧弹出</Cell>
        <Cell arrow onClick={() => { setRightPopup(true); }}>右侧弹出</Cell>
      </CellGroup>
      <CellGroup title="关闭按钮">
        <Cell arrow onClick={() => { setCloseIconPopup(true); }}>关闭按钮</Cell>
        <Cell arrow onClick={() => { setCustomizeIconPopup(true); }}>自定义按钮</Cell>
        <Cell arrow onClick={() => { setPostionIconPopup(true); }}>图标位置</Cell>
      </CellGroup>
      <CellGroup title="圆角弹窗">
        <Cell arrow onClick={() => { setRoundBottomPopup(true); }}>开启底部位置圆角弹窗</Cell>
        <Cell arrow onClick={() => { setRoundTopPopup(true); }}>开启顶部位置圆角弹窗</Cell>
        <Cell arrow onClick={() => { setRoundLeftPopup(true); }}>开启左侧位置圆角弹窗</Cell>
        <Cell arrow onClick={() => { setRoundRightPopup(true); }}>开启右侧位置圆角弹窗</Cell>
      </CellGroup>
      {/* 基础用法 */}
      <Popup show={basisPopup} style={{ padding: '30px 50px', borderRadius: '12px' }} onClose={() => { onClosePopup(EPopupType.basis); }}>内容</Popup>
      {/* 弹出位置 */}
      <Popup show={topPopup} postion="top" style={{ height: '30%' }} onClose={() => { onClosePopup(EPopupType.top); }}></Popup>
      <Popup show={bottomPopup} postion="bottom" style={{ height: '30%' }} onClose={() => { onClosePopup(EPopupType.bottom); }}></Popup>
      <Popup show={leftPopup} postion="left" style={{ height: '100%', width: '30%' }} onClose={() => { onClosePopup(EPopupType.left); }}></Popup>
      <Popup show={rightPopup} postion="right" style={{ height: '100%', width: '30%' }} onClose={() => { onClosePopup(EPopupType.right); }}></Popup>
      {/* 关闭按钮 */}
      <Popup show={closeIconPopup} closeable round postion="bottom" style={{ height: '30%' }} onClose={() => { onClosePopup(EPopupType.closeIcon); }}></Popup>
      <Popup show={customizeIconPopup} closeable closeIcon="close" round postion="bottom" style={{ height: '30%' }} onClose={() => { onClosePopup(EPopupType.customizeIcon); }}></Popup>
      <Popup show={postionIconPopup} closeable closeIcon="close" closeIconPosition="top-left" round postion="bottom" style={{ height: '30%' }} onClose={() => { onClosePopup(EPopupType.postionIcon); }}></Popup>
      {/* 圆角弹窗 */}
      <Popup show={roundBottomPopup} round postion="bottom" style={{ height: '30%' }} onClose={() => { onClosePopup(EPopupType.roundBottom); }}></Popup>
      <Popup show={roundTopPopup} round postion="top" style={{ height: '30%' }} onClose={() => { onClosePopup(EPopupType.roundTop); }}></Popup>
      <Popup show={roundLeftPopup} round postion="left" style={{ height: '100%', width: '30%' }} onClose={() => { onClosePopup(EPopupType.roundLeft); }}></Popup>
      <Popup show={roundRightPopup} round postion="right" style={{ height: '100%', width: '30%' }} onClose={() => { onClosePopup(EPopupType.roundRight); }}></Popup>
    </>
  );
};

export default PopupDemo;