import React, { CSSProperties, FunctionComponent, useState } from 'react';
import './style';
import { addUnit, inBrowser, isDef } from '../../../../common/utils/utils';
import { Icon, LazyLoad } from '../../../../index';

interface IProps {
  // 自定义Image组件样式类名
  className?: string;
  // 自定义Image组件样式
  style?: CSSProperties;
  // 定义图片宽度
  width?: number | string;
  // 定义图片高度
  height?: number | string;
  // 图片地址
  src?: string;
  // 替代文本
  alt?: string;
  // 图片填充模式
  fit?: 'contain' | 'cover' | 'fill' | 'none' | 'scale-down';
  // 是否圆角
  round?: boolean;
  // 圆角大小
  radius?: number | string | null;
  // 是否开启懒加载
  lazyLoad?: boolean;
  // 是否显示加载中状态提示
  showLoading?: boolean;
  // 加载中状态提示图标名称或链接
  loadingIcon?: string;
  // 自定义加载中状态提示
  loadingCustomize?: React.ReactNode;
  // 是否显示加载失败状态提示
  showError?: boolean;
  // 加载失败状态提示图标名称或链接
  errorIcon?: string;
  // 自定义加载失败状态提示
  errorCustomize?: React.ReactNode;
  // 点击图片事件
  onClick?: () => void;
}

const Image: FunctionComponent<IProps> = (props) => {
  // 图片是否正在加载
  const [isLoading, setIsLoading] = useState<boolean>(true);
  // 图片是否加载失败
  const [isError, setIsError] = useState<boolean>(false);
  const {
    className = '',
    style = {},
    width = 80,
    height = 80,
    src,
    alt,
    fit = 'fill',
    round = false,
    radius = null,
    lazyLoad = false,
    showLoading = true,
    loadingIcon = 'photo',
    loadingCustomize,
    showError = true,
    errorIcon = 'photo-fail',
    errorCustomize,
    onClick = () => { }
  } = props;
  // 获取自定义样式
  const getStyle = () => {
    const _style: CSSProperties = {};
    if (isDef(width)) {
      _style.width = addUnit(width);
    }
    if (isDef(height)) {
      _style.height = addUnit(height);
    }
    if (isDef(radius)) {
      _style.overflow = 'hidden';
      _style.borderRadius = addUnit(radius);
    }
    return { ..._style, ...style };
  };
  // 获取加载中状态提示
  const renderLoadingIcon = () => {
    if (loadingCustomize) {
      return loadingCustomize;
    }
    return (
      <Icon
        name={loadingIcon}
        className={'image_loading_icon'}
      />
    );
  };
  // 获取加载失败状态提示
  const renderErrorIcon = () => {
    if (errorCustomize) {
      return errorCustomize;
    }
    return (
      <Icon
        name={errorIcon}
        className={'image_error_icon'}
      />
    );
  };
  // 获取加载中状态或加载失败状态占位图
  const renderPlaceholder = () => {
    if (isLoading && showLoading && inBrowser) {
      return (
        <div className={'image_loading'}>{renderLoadingIcon()}</div>
      );
    }
    if (isError && showError) {
      return (
        <div className={'image_error'}>{renderErrorIcon()}</div>
      );
    }
    return;
  };
  // 获取img内容
  const renderImg = () => {
    if (isError || !src) {
      return;
    }
    const attrs = {
      alt,
      className: 'image_img',
      style: {
        objectFit: fit,
      },
    };
    if (lazyLoad) {
      return (
        <LazyLoad height={height} width={width}>
          <img src={src} {...attrs} onLoad={() => { setIsLoading(false); }} onError={() => { setIsLoading(false); setIsError(true); }} />
        </LazyLoad>
      );
    }
    return <img src={src} {...attrs} onLoad={() => { setIsLoading(false); }} onError={() => { setIsLoading(false); setIsError(true); }} />;
  };
  return (
    <div className={['image_swapper', round ? 'image_round' : '', className].filter((v) => v).join(' ')} style={getStyle()} onClick={() => { onClick(); }}>
      {renderImg()}
      {renderPlaceholder()}
      {props.children}
    </div>
  );
};

export default Image;