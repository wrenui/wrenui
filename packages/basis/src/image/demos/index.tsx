import React, { FunctionComponent } from 'react';
import styles from './demo.module.less';
import { Image, Loading } from '../../../../../index';
import { imgUrl, imgUrl_error } from '../../../../../common/assets/data/index';

interface IProps {

}

const ImageDemo: FunctionComponent<IProps> = (props) => {
  return (
    <>
      <div className={styles.demo_box}>
        <h2>基础用法</h2>
        <div className={styles.demo_item_box}>
          <Image src={imgUrl} width={120} height={120}></Image>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>填充模式</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="contain"></Image>
            <span>contain</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="cover"></Image>
            <span>cover</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="fill"></Image>
            <span>fill</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="none"></Image>
            <span>none</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="scale-down"></Image>
            <span>scale-down</span>
          </div>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>圆角图片</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="contain" round></Image>
            <span>contain</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="cover" round></Image>
            <span>cover</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="fill" round></Image>
            <span>fill</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="none" round></Image>
            <span>none</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} fit="scale-down" round></Image>
            <span>scale-down</span>
          </div>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>开启懒加载</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} lazyLoad></Image>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} lazyLoad></Image>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl} width={120} height={120} lazyLoad></Image>
          </div>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>加载中状态提示</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.demo_item}>
            <Image width={120} height={120}></Image>
            <span>默认提示</span>
          </div>
          <div className={styles.demo_item}>
            <Image width={120} height={120} loadingCustomize={<Loading size={20}></Loading>}></Image>
            <span>自定义提示图标</span>
          </div>
          <div className={styles.demo_item}>
            <Image width={120} height={120} loadingCustomize={<Loading type="spinner" size={20} content="加载中..." vertical></Loading>}></Image>
            <span>图标下方提示文字</span>
          </div>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>加载失败状态提示</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.demo_item}>
            <Image src={imgUrl_error} width={120} height={120}></Image>
            <span>默认提示</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl_error} width={120} height={120} errorIcon="enlarge"></Image>
            <span>自定义提示按钮</span>
          </div>
          <div className={styles.demo_item}>
            <Image src={imgUrl_error} width={120} height={120} errorCustomize={<span>加载失败</span>}></Image>
            <span>自定义提示内容</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default ImageDemo;