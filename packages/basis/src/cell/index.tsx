import React, { CSSProperties, FunctionComponent } from 'react';
import './style';
import { Icon } from '../../../../index';
import { isDef } from '../../../../common/utils/utils';

interface IProps {
  // 自定义Cell样式类名
  className?: string;
  // 自定义Cell样式内容
  style?: CSSProperties;
  // 自定义Title样式类名
  titleClassName?: string;
  // 自定义Title样式内容
  titleStyle?: CSSProperties;
  // 自定义label样式类名
  labelClassName?: string;
  // 自定义label样式内容
  labelStyle?: CSSProperties;
  // 自定义value样式类名
  valueClassName?: string;
  // 自定义value样式内容
  valueStyle?: CSSProperties;
  // 左侧标题
  title?: React.ReactNode;
  // 右侧内容
  value?: React.ReactNode;
  // 标题下说明
  label?: React.ReactNode;
  // 单元格大小
  size?: 'normal' | 'large';
  // title左侧图标
  icon?: string;
  // 自定义左侧icon样式类名
  leftIconClassName?: string;
  // 自定义左侧icon样式内容
  leftIconStyle?: CSSProperties;
  // title左侧图标大小
  iconSize?: number | string;
  // title左侧图标颜色
  iconColor?: string;
  // 是否显示右侧箭头
  arrow?: boolean;
  // 右侧箭头方向
  arrowDirection?: 'up' | 'down' | 'left' | 'right';
  // 自定义右侧图标
  rightIcon?: string;
  // 自定义右侧icon样式类名
  rightIconClassName?: string;
  // 自定义右侧icon样式内容
  rightIconStyle?: CSSProperties;
  // 右侧自定义图标大小
  rightIconSize?: number | string;
  // 右侧自定义图标颜色
  rightIconColor?: string;
  // 左右内容是否垂直居中
  center?: boolean;
  // 是否显示边框
  border?: boolean;
  // 是否开启点击css样式，开启后仅在arrow为true时生效
  clickable?: boolean;
  // 是否开启表单验证必选星号
  required?: boolean;
  // 单元格点击事件
  onClick?: () => void;
}

const Cell: FunctionComponent<IProps> = (props) => {
  const {
    className = '',
    style = {},
    titleClassName = '',
    titleStyle = {},
    labelClassName = '',
    labelStyle = {},
    valueClassName = '',
    valueStyle = {},
    title,
    value,
    label,
    size = 'normal',
    icon,
    leftIconClassName = '',
    leftIconStyle = {},
    iconColor,
    iconSize = 16,
    arrow,
    arrowDirection = 'right',
    rightIcon,
    rightIconClassName = '',
    rightIconStyle = {},
    rightIconColor,
    rightIconSize = 16,
    center = false,
    border = true,
    clickable = true,
    required = false,
    onClick = () => { }
  } = props;
  // 获取cell样式类名
  const getCellClassName = () => {
    const sizeClass = size === 'large' ? `cell_${size}` : '';
    const centerClass = center ? 'cell_center' : '';
    const borderClass = !border ? 'cell_no_border' : '';
    const clickableClass = clickable && arrow ? 'cell_clickable' : '';
    const requiredClass = required ? 'cell_required' : '';
    const _className = ['cell_wrapper', sizeClass, centerClass, borderClass, clickableClass, requiredClass, className];
    return _className.filter((v) => v).join(' ');
  };
  // 获取左侧标题
  const renderTitle = () => {
    if (isDef(title)) {
      return (
        <div className={['cell_title', titleClassName].filter((v) => v).join(' ')} style={titleStyle}>
          <span>{title}</span>
          {renderLabel()}
        </div>
      );
    }
    return;
  };
  // 获取左侧标题说明
  const renderLabel = () => {
    if (title && isDef(label)) {
      return (
        <div className={['cell_label', labelClassName].filter((v) => v).join(' ')} style={labelStyle}>
          {label}
        </div>
      );
    }
    return;
  };
  // 获取右侧内容
  const renderValue = () => {
    if (isDef(value) || props.children) {
      return (
        <div className={['cell_value', !title ? 'cell_value_alone' : '', valueClassName].filter((v) => v).join(' ')} style={valueStyle}>
          {props.children ? props.children : <span>{value}</span>}
        </div>
      );
    }
    return;
  };
  // 获取title左侧图标
  const renderLeftIcon = () => {
    if (icon) {
      return <Icon
        className={[leftIconClassName, 'cell_left_icon'].filter((v) => v).join(' ')}
        style={leftIconStyle}
        name={icon}
        color={iconColor}
        size={iconSize}
      />;
    }
    return;
  };
  // 获取value右侧图标
  const renderRightIcon = () => {
    if (!rightIcon && arrow) {
      return <Icon
        className={['cell_right_icon', rightIconClassName].filter((v) => v).join(' ')}
        style={rightIconStyle}
        name={arrowDirection === 'right' ? 'arrow' : `arrow-${arrowDirection}`}
        size={rightIconSize}
        color={rightIconColor}
      />;
    }
    if (rightIcon) {
      return <Icon
        className={['cell_right_icon', rightIconClassName].filter((v) => v).join(' ')}
        style={rightIconStyle}
        name={rightIcon}
        size={rightIconSize}
        color={rightIconColor}
      />;
    }
    return;
  };
  return (
    <div
      className={getCellClassName()}
      style={style}
      onClick={() => { onClick(); }}
    >
      {renderLeftIcon()}
      {renderTitle()}
      {renderValue()}
      {renderRightIcon()}
    </div>
  );
};

export default Cell;