import React, { FunctionComponent } from 'react';
import styles from './demo.module.less';

import { CellGroup, Cell, Tag } from '../../../../../index';

interface IProps {

}

const CellDemo: FunctionComponent<IProps> = (props) => {
  return (
    <>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>基础用法</h2>
        <CellGroup>
          <Cell
            title="这是标题"
            value="这是内容"
          />
          <Cell
            title="这是标题"
            label="这是标题说明"
            value="这是内容"
          />
        </CellGroup>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>单元格大小</h2>
        <CellGroup>
          <Cell
            title="这是标题"
            value="这是内容"
            size="large"
          />
          <Cell
            title="这是标题"
            label="这是标题说明"
            value="这是内容"
            size="large"
          />
        </CellGroup>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>展示标签左侧图标</h2>
        <CellGroup>
          <Cell
            title="定位"
            value="这是内容"
            icon="location-o"
          />
          <Cell
            title={<span>这是带有标签的标题 <Tag type="success">标签</Tag></span>}
            value="这是内容"
          />
        </CellGroup>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>仅设置内容</h2>
        <Cell
          value="仅设置内容"
        />
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>展示内容右侧箭头及箭头方向</h2>
        <CellGroup>
          <Cell
            title="这是标题"
            arrow
          />
          <Cell
            title="这是标题"
            value="箭头向右"
            arrow
          />
          <Cell
            title="这是标题"
            value="箭头向下"
            arrow
            arrowDirection="down"
          />
          <Cell
            title="这是标题"
            value="箭头向左"
            arrow
            arrowDirection="left"
          />
          <Cell
            title="这是标题"
            value="箭头向上"
            arrow
            arrowDirection="up"
          />
        </CellGroup>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>自定义内容右侧图标</h2>
        <CellGroup>
          <Cell
            title="这是标题"
            value="收藏"
            rightIcon="like-o"
          />
          <Cell
            title="这是标题"
            value="设置"
            rightIcon="setting-o"
          />
        </CellGroup>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>设置必填表单星号</h2>
        <CellGroup>
          <Cell
            title="这是必填表单"
            value="内容"
            required
          />
          <Cell
            title="这是必填表单"
            value="内容"
            required
          />
        </CellGroup>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>分组设置标题</h2>
        <CellGroup title="分组 1">
          <Cell
            title="分组 1 标题"
            value="分组 1 内容"
          />
        </CellGroup>
        <CellGroup title="分组 2">
          <Cell
            title="分组 2 标题"
            value="分组 2 内容"
          />
          <Cell
            title="分组 2 标题"
            value="分组 2 内容"
          />
        </CellGroup>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>垂直居中</h2>
        <CellGroup>
          <Cell
            title="这是标题"
            label="这是标题说明"
            value="这是内容"
            center
          />
          <Cell
            title="这是标题"
            label="这是标题说明"
            value="这是内容"
            center
          />
        </CellGroup>
      </div>
    </>
  );
};

export default CellDemo;