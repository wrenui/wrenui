import React, { CSSProperties, FunctionComponent } from 'react';
import './style';

interface IProps {
  // 自定义单元格组样式类名
  className?: string;
  // 自定义单元格组样式
  style?: CSSProperties;
  // CellGroup标题
  title?: React.ReactNode;
}

const CellGroup: FunctionComponent<IProps> = (props) => {
  const {
    children,
    className = '',
    style = {},
    title
  } = props;
  // 获取Cell组内容
  const renderCellGroup = () => {
    return (
      <div className={['cell_group_wrapper', className].filter((v) => v).join(' ')} style={style}>
        {children}
      </div>
    );
  };
  return (
    title ?
      <>
        <div className={'cell_group_title'}>{title}</div>
        {renderCellGroup()}
      </> :
      renderCellGroup()
  );
};

export default CellGroup;