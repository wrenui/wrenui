import React, { FunctionComponent } from 'react';
import styles from './demo.module.less';

import { CellGroup, Cell } from '../../../../../index';

interface IProps {

}

const CellDemo: FunctionComponent<IProps> = (props) => {
  return (
    <>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>基础用法</h2>
        <CellGroup>
          <Cell
            title="这是标题"
            value="这是内容"
          />
          <Cell
            title="这是标题"
            label="这是标题说明"
            value="这是内容"
          />
        </CellGroup>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>设置单元格分组标题</h2>
        <CellGroup title="这是分组标题">
          <Cell
            title="这是标题"
            value="这是内容"
          />
          <Cell
            title="这是标题"
            value="这是内容"
          />
        </CellGroup>
      </div>
    </>
  );
};

export default CellDemo;