import React, { CSSProperties, FunctionComponent, useEffect } from 'react';
import '../../style';
import { Popup, Icon, Loading } from '../../../../../../index';
import { EToastType, getIcon } from '../../params';

interface IProps {
  show: boolean;
  notice: {
    // 提示类型
    type?: 'info' | 'prompt' | 'success' | 'fail' | 'loading';
    // 提示文字
    message?: React.ReactNode;
  };
  // 提示显示时常，默认2000毫秒
  duration?: number;
  options: {
    // 自定义Toast提示样式类名
    className?: string;
    // 自定义Toast提示样式
    style?: CSSProperties;
    // 自定义弹出提示样式层级
    zIndex?: number | string;
    // 是否锁定提示弹出层下背景滚动
    isLockScroll?: boolean;
    // 自定义弹出提示过渡动画样式类名
    transition?: string;
    // 自定义弹出提示位置
    position?: 'center' | 'top' | 'bottom';
    // 自定义提示图标
    icon?: string | null;
    // 自定义提示图标大小
    iconSize?: number | string;
    // 加载提示图标类型
    loadingIconType?: 'circular' | 'spinner';
    // 是否开启点击后关闭
    closeOnClick?: boolean;
    // 是否开启点击遮罩层后关闭
    closeOnClickOverlay?: boolean;
  };
  // 关闭提示事件
  onClose?: () => void;
}

const Notice: FunctionComponent<IProps> = (props) => {
  const {
    show = false,
    options: {
      className = '',
      style = {},
      zIndex = 2000,
      isLockScroll = false,
      transition = 'fade',
      position = 'center',
      icon,
      iconSize = 42,
      loadingIconType = 'circular',
      closeOnClick = false,
      closeOnClickOverlay = false,
    },
    notice: {
      type = 'info',
      message = type === EToastType.loading ? '加载中...' : '',
    },
    duration = 2000,
    onClose = () => { }
  } = props;
  // 定时器
  let _timer: any = null;
  // 监听show
  useEffect(() => {
    if (duration > 0) {
      _timer = setTimeout(() => {
        clearToast();
      }, duration);
    }
  }, []);

  // 关闭消息提示
  const clearToast = () => {
    // 清除定时器
    clearTimeout(_timer);
    onClose();
  };

  // 获取提示文字
  const renderMessage = () => {
    if (message) {
      return <div className={'toast_message'}>{message}</div>;
    }
    return;
  };
  // 获取提示图标
  const renderIcon = () => {
    if (icon || type === EToastType.success || type === EToastType.fail || type === EToastType.prompt) {
      return <Icon name={icon ? icon : getIcon(type)} size={iconSize}></Icon>;
    }
    if (type === EToastType.loading) {
      return <Loading className={'toast_loading'} type={loadingIconType}></Loading>;
    }
    return;
  };
  // 关闭提示
  const closeToast = (clostType: 'toast' | 'overlay') => {
    if (type === 'loading') {
      return;
    }
    if ((clostType === 'toast' && closeOnClick) || (clostType === 'overlay' && closeOnClickOverlay)) {
      clearToast();
    }
  };
  return (
    <Popup
      show={show}
      className={[`toast_wrapper_${type}`, `toast_wrapper_${position}`, className].filter((v) => v).join(' ')}
      style={style}
      zIndex={zIndex}
      overlayShow={true}
      overlayClassName={'overlay_toast'}
      closeOnClickOverlay={closeOnClickOverlay}
      transition={transition}
      isLockScroll={isLockScroll}
      onClick={() => { closeToast('toast'); }}
      onClose={() => { closeToast('overlay'); }}
    >
      {renderIcon()}
      {renderMessage()}
    </Popup>
  );
};

export default Notice;