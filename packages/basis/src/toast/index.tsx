import React from 'react';
import ReactDOM from 'react-dom';
import Notice from './components/notice';
import { EToastType, IOptions } from './params';

// 节流，当页面中有Toast提示时，无法打开第二个Toast，直到上一个Toast关闭。
let hasToast: boolean = false;
// 暂存Toast参数
let toastParams: any = {};
interface IProps {
  type: 'info' | 'prompt' | 'success' | 'fail' | 'loading' | 'clear';
  message?: React.ReactNode;
  duration?: number;
  onClose?: () => void;
  options?: IOptions;
}
const createNotication = (props: IProps) => {
  const {
    type,
    message,
    duration,
    onClose = () => { },
    options = {},
  } = props;
  if ((hasToast && type !== EToastType.clear) || (type === EToastType.clear && toastParams.type !== EToastType.loading) || (type !== EToastType.loading && Number(duration) <= 0)) {
    return;
  }
  const _div = document.createElement('div');
  _div.id = 'wren_ui_toast';
  if (type !== EToastType.clear) {
    document.body.appendChild(_div);
  }
  const closeToast = () => {
    toRender(_div, false);
    setTimeout(() => {
      ReactDOM.unmountComponentAtNode(_div);
      if (document.getElementById('wren_ui_toast')) {
        const div_remove: any = document.getElementById('wren_ui_toast');
        document.body.removeChild(div_remove);
      }
      if (onClose) {
        onClose();
      }
      hasToast = false;
    }, 380);
  };
  // 重新渲染dom
  const toRender = (_div: any, display: boolean) => {
    const params = {
      show: display,
      notice: {
        type: display ? type : toastParams.type,
        message: display ? message : toastParams.message
      },
      duration: display ? duration : toastParams.duration,
      options: display ? options : toastParams.options,
      onClose: () => { if (display && document.getElementById('wren_ui_toast')) { closeToast(); } },
    };
    ReactDOM.render(React.createElement(Notice, params), display ? _div : document.getElementById('wren_ui_toast'));
  };
  // 如果是clear就清除Toast
  if (type === EToastType.clear && toastParams?.type === EToastType.loading) {
    closeToast();
    return;
  }
  toastParams = JSON.parse(JSON.stringify({ type, message, duration, options }));
  // 非clear时渲染Toast组件dom
  hasToast = true;
  toRender(_div, true);
};

export default {
  info(message = '提示信息', duration = 2000, onClose = () => { }, options = {}) {
    return createNotication({ type: EToastType.info, message, duration, onClose, options });
  },
  prompt(message = '常规提示', duration = 2000, onClose = () => { }, options: {}) {
    return createNotication({ type: EToastType.prompt, message, duration, onClose, options });
  },
  success(message = '操作成功', duration = 2000, onClose = () => { }, options = {}) {
    return createNotication({ type: EToastType.success, message, duration, onClose, options });
  },
  fail(message = '操作失败', duration = 2000, onClose = () => { }, options = {}) {
    return createNotication({ type: EToastType.fail, message, duration, onClose, options });
  },
  loading(message = '加载中...', duration = 0, onClose = () => { }, options = {}) {
    return createNotication({ type: EToastType.loading, message, duration, onClose, options });
  },
  clear(onClose = () => { }) {
    return createNotication({ type: EToastType.clear, onClose });
  }
};