import React, { FunctionComponent } from 'react';
import styles from './demo.module.less';
import { Cell, CellGroup, Toast } from '../../../../../index';
import { EToastType } from './params';
import { imgUrl } from '../../../../../common/assets/data/index';

interface IProps {

}

const ToastDemo: FunctionComponent<IProps> = (props) => {
  // 打开Toast提示
  const openToast = (type: string) => {
    switch (type) {
      default:
      case EToastType.info:
        Toast.info('这是一条信息');
        break;
      case EToastType.prompt:
        Toast.prompt('常规提示');
        break;
      case EToastType.success:
        Toast.success('操作成功');
        break;
      case EToastType.fail:
        Toast.fail('操作失败');
        break;
      case EToastType.loading:
        Toast.loading('加载中...');
        setTimeout(() => {
          Toast.clear();
        }, 4000);
        break;
      case EToastType.self_icon:
        Toast.prompt('自定义图标', 2000, () => { }, { icon: 'fire-o' });
        break;
      case EToastType.self_icon_size:
        Toast.prompt('图标大小', 2000, () => { }, { icon: 'fire-o', iconSize: 28 });
        break;
      case EToastType.self_icon_url:
        Toast.prompt('Url图片', 2000, () => { }, { icon: imgUrl });
        break;
      case EToastType.self_loading:
        Toast.loading('自定义加载', 0, () => { }, { loadingIconType: 'spinner' });
        setTimeout(() => {
          Toast.clear();
        }, 4000);
        break;
      case EToastType.self_top:
        Toast.info('顶部提示消息', 2000, () => { }, { position: 'top' });
        break;
      case EToastType.self_bottom:
        Toast.info('底部提示消息', 2000, () => { }, { position: 'bottom' });
        break;
      case EToastType.self_close:
        Toast.success('点击可关闭', 2000, () => { }, { closeOnClick: true });
        break;
      case EToastType.self_overlay_close:
        Toast.fail('点击遮罩层', 2000, () => { }, { closeOnClickOverlay: true });
        break;
    }
  };
  return (
    <>
      <div className={styles.demo_box}>
        <h2>基础用法</h2>
        <div className={[styles.demo_item_box, styles.demo_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell arrow onClick={() => { openToast(EToastType.info); }}>info 信息提示</Cell>
            <Cell arrow onClick={() => { openToast(EToastType.prompt); }}>prompt 常规提示</Cell>
            <Cell arrow onClick={() => { openToast(EToastType.success); }}>success 成功提示</Cell>
            <Cell arrow onClick={() => { openToast(EToastType.fail); }}>fail 失败提示</Cell>
            <Cell arrow onClick={() => { openToast(EToastType.loading); }}>loading 加载中提示</Cell>
          </CellGroup>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>自定义图标</h2>
        <div className={[styles.demo_item_box, styles.demo_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell arrow onClick={() => { openToast(EToastType.self_icon); }}>自定义图标</Cell>
            <Cell arrow onClick={() => { openToast(EToastType.self_icon_size); }}>自定义图标大小</Cell>
            <Cell arrow onClick={() => { openToast(EToastType.self_icon_url); }}>自定义Url图片</Cell>
            <Cell arrow onClick={() => { openToast(EToastType.self_loading); }}>自定义加载图标</Cell>
          </CellGroup>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>自定义展示位置</h2>
        <div className={[styles.demo_item_box, styles.demo_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell arrow onClick={() => { openToast(EToastType.self_top); }}>顶部展示</Cell>
            <Cell arrow onClick={() => { openToast(EToastType.self_bottom); }}>底部展示</Cell>
          </CellGroup>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>点击提示消息后关闭</h2>
        <div className={[styles.demo_item_box, styles.demo_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell arrow onClick={() => { openToast(EToastType.self_close); }}>点击关闭</Cell>
          </CellGroup>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>点击遮罩层后关闭</h2>
        <div className={[styles.demo_item_box, styles.demo_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell arrow onClick={() => { openToast(EToastType.self_overlay_close); }}>点击遮罩层关闭</Cell>
          </CellGroup>
        </div>
      </div>
    </>
  );
};

export default ToastDemo;