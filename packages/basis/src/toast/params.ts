import { CSSProperties } from 'react';

export interface IOptions {
  // 自定义Toast提示样式类名
  className?: string;
  // 自定义Toast提示样式
  style?: CSSProperties;
  // 自定义弹出提示样式层级
  zIndex?: number | string;
  // 是否锁定提示弹出层下背景滚动
  isLockScroll?: boolean;
  // 是否允许点击提示层下方内容
  isBidClick?: boolean;
  // 自定义弹出提示过渡动画样式类名
  transition?: string;
  // 自定义弹出提示位置
  position?: 'center' | 'top' | 'bottom';
  // 自定义提示图标
  icon?: string | null;
  // 自定义提示图标大小
  iconSize?: number | string;
  // 加载提示图标类型
  loadingIconType?: 'circular' | 'spinner';
  // 是否开启点击后关闭
  closeOnClick?: boolean;
  // 是否开启点击遮罩层后关闭(仅在isBidClick为true情况下生效)
  closeOnClickOverlay?: boolean;
}

export enum EToastType {
  info = 'info',
  prompt = 'prompt',
  success = 'success',
  fail = 'fail',
  loading = 'loading',
  clear = 'clear',
}

export const getIcon = (type: 'info' | 'prompt' | 'success' | 'fail' | 'loading') => {
  const iconList = {
    prompt: 'envelop-o',
    success: 'success',
    fail: 'cross'
  };
  return iconList[type];
};