import React, { FunctionComponent, useState } from 'react';
import '../style';
import styles from './demo.module.less';
import { Icon } from '../../../../../index';
import icons from '../../../../../common/assets/icon/icon.config';
import { copyIcon } from '../../../../../common/utils/utils';
import { IIconProps } from './params';

interface IProps {

}

const IconDemo: FunctionComponent<IProps> = (props) => {
  // 基础图标数据
  const [basisIconList] = useState<any>(icons.basic);
  // 线框图标数据
  const [outlineIconList] = useState<any>(icons.outline);
  // 实底图标数据
  const [filledIconList] = useState<any>(icons.filled);
  const [basicIcon1] = useState<string>('chat-o');
  const [basicIcon2] = useState<string>('setting-o');
  const [basicIcon3] = useState<string>('cart-o');
  const [basicIcon4] = useState<string>('friends-o');
  const [basicIcon5] = useState<string>('more');
  const [basicIcon6] = useState<string>('fire');
  const [imageUrl] = useState<string>('https://b.yzcdn.cn/vant/icon-demo-1126.png');
  // 获取图标组件
  const getIconName = (name: string, options: IIconProps = {}) => {
    if (options.dot) {
      return <Icon className={styles.icon} name={name} dot />;
    }
    if (options.badge) {
      return <Icon className={styles.icon} name={name} badge={options.badge} />;
    }
    if (options.content) {
      return <Icon className={styles.icon} name={name} content={options.content} />;
    }
    if (options.color) {
      return <Icon className={styles.icon} name={name} color={options.color} />;
    }
    if (options.size) {
      return <Icon className={styles.icon} name={name} size={options.size} />;
    }
    return <Icon className={styles.icon} name={name} />;
  };
  return (
    <>
      <div className={styles.demo_box}>
        <h1 style={{margin: '0'}}>用法示例</h1>
        <h2>基础用法</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.icon_box}>
            <div className={styles.icon_item} key={`basic_${basicIcon1}`} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon1)); }}>
              {getIconName(basicIcon1)}
              <span className={styles.icon_name}>{basicIcon1}</span>
            </div>
            <div className={styles.icon_item} key={`basic_${basicIcon2}`} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon2)); }}>
              {getIconName(basicIcon2)}
              <span className={styles.icon_name}>{basicIcon2}</span>
            </div>
            <div className={styles.icon_item} key={`basic_${basicIcon3}`} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon3)); }}>
              {getIconName(basicIcon3)}
              <span className={styles.icon_name}>{basicIcon3}</span>
            </div>
            <div className={styles.icon_item} key={`basic_${basicIcon4}`} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon4)); }}>
              {getIconName(basicIcon4)}
              <span className={styles.icon_name}>{basicIcon4}</span>
            </div>
          </div>
          <div className={styles.icon_box}>
            <div className={styles.icon_item} key={`_basic_${basicIcon1}`} onClick={() => { console.log('已拷贝图标组件：', copyIcon(imageUrl)); }}>
              {getIconName(imageUrl)}
              <span className={styles.icon_name}>url图片示例<br />尺寸：默认32px</span>
            </div>
            <div className={styles.icon_item} key={`_basic_${basicIcon2}`} onClick={() => { console.log('已拷贝图标组件：', copyIcon(imageUrl, { size: 38 })); }}>
              {getIconName(imageUrl, { size: 38 })}
              <span className={styles.icon_name}>url图片示例<br />尺寸：38px</span>
            </div>
            <div className={styles.icon_item} key={`_basic_${basicIcon3}`} onClick={() => { console.log('已拷贝图标组件：', copyIcon(imageUrl, { size: '2.5rem' })); }}>
              {getIconName(imageUrl, { size: '2.5rem' })}
              <span className={styles.icon_name}>url图片示例<br />尺寸：2.5rem</span>
            </div>
            <div className={styles.icon_item} key={`_basic_${basicIcon4}`} onClick={() => { console.log('已拷贝图标组件：', copyIcon(imageUrl, { size: '1.8em' })); }}>
              {getIconName(imageUrl, { size: '1.8em' })}
              <span className={styles.icon_name}>url图片示例<br />尺寸：1.8em</span>
            </div>
          </div>
        </div>

        <h2>徽标提示</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.icon_box}>
            <div className={[styles.demo_item_box, styles.icon_item_box_badge].filter((v) => v).join(' ')}>
              <div className={styles.icon_item_badge} key={basicIcon1} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon1, { dot: true })); }}>
                {getIconName(basicIcon1, { dot: true })}
              </div>
              <div className={styles.icon_item_badge} key={basicIcon2} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon2, { content: '<span>设置</span>' })); }}>
                {getIconName(basicIcon2, { content: <span>设置</span> })}
              </div>
              <div className={styles.icon_item_badge} key={basicIcon3} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon3, { badge: 9 })); }}>
                {getIconName(basicIcon3, { badge: 9 })}
              </div>
              <div className={styles.icon_item_badge} key={basicIcon4} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon4, { badge: 99 })); }}>
                {getIconName(basicIcon4, { badge: 99 })}
              </div>
            </div>
          </div>
        </div>

        <h2>图标颜色</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.icon_box}>
            <div className={styles.icon_item} key={basicIcon1} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon1, { color: '#2d8cf0' })); }}>
              {getIconName(basicIcon1, { color: '#2d8cf0' })}
              <span className={styles.icon_name}>{basicIcon1}</span>
            </div>
            <div className={styles.icon_item} key={basicIcon2} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon2, { color: '#19be6b' })); }}>
              {getIconName(basicIcon2, { color: '#19be6b' })}
              <span className={styles.icon_name}>{basicIcon2}</span>
            </div>
            <div className={styles.icon_item} key={basicIcon5} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon5, { color: '#ff9900' })); }}>
              {getIconName(basicIcon5, { color: '#ff9900' })}
              <span className={styles.icon_name}>{basicIcon5}</span>
            </div>
            <div className={styles.icon_item} key={basicIcon6} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon6, { color: '#ed4014' })); }}>
              {getIconName(basicIcon6, { color: '#ed4014' })}
              <span className={styles.icon_name}>{basicIcon6}</span>
            </div>
          </div>
        </div>

        <h2>图标大小</h2>
        <div className={styles.demo_item_box}>
          <div className={styles.icon_box}>
            <div className={styles.icon_item} key={basicIcon1} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon1, { size: 40 })); }}>
              {getIconName(basicIcon1, { size: 40 })}
              <span className={styles.icon_name}>{basicIcon1}</span>
            </div>
            <div className={styles.icon_item} key={basicIcon2} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon2, { size: 32 })); }}>
              {getIconName(basicIcon2, { size: 32 })}
              <span className={styles.icon_name}>{basicIcon2}</span>
            </div>
            <div className={styles.icon_item} key={basicIcon3} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon3, { size: 24 })); }}>
              {getIconName(basicIcon3, { size: 24 })}
              <span className={styles.icon_name}>{basicIcon3}</span>
            </div>
            <div className={styles.icon_item} key={basicIcon4} onClick={() => { console.log('已拷贝图标组件：', copyIcon(basicIcon4, { size: 16 })); }}>
              {getIconName(basicIcon4, { size: 16 })}
              <span className={styles.icon_name}>{basicIcon4}</span>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h1>基础图标</h1>
        <div className={styles.demo_item_box}>
          <div className={styles.icon_box}>
            {basisIconList.map((name: string) => {
              return (
                <div className={styles.icon_item} key={name} onClick={() => { console.log('已拷贝图标组件：', copyIcon(name, { size: 32 })); }}>
                  {getIconName(name)}
                  <span className={styles.icon_name}>{name}</span>
                </div>);
            })}
          </div>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h1>线框图标</h1>
        <div className={styles.demo_item_box}>
          <div className={styles.icon_box}>
            {outlineIconList.map((name: string) => {
              return (
                <div className={styles.icon_item} key={name} onClick={() => { console.log('已拷贝图标组件：', copyIcon(name, { size: 32 })); }}>
                  {getIconName(name)}
                  <span className={styles.icon_name}>{name}</span>
                </div>);
            })}
          </div>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h1>实底图标</h1>
        <div className={styles.demo_item_box}>
          <div className={styles.icon_box}>
            {filledIconList.map((name: string) => {
              return (
                <div className={styles.icon_item} key={name} onClick={() => { console.log('已拷贝图标组件：', copyIcon(name, { size: 32 })); }}>
                  {getIconName(name)}
                  <span className={styles.icon_name}>{name}</span>
                </div>);
            })}
          </div>
        </div>
      </div>
    </>

  );
};

export default IconDemo;