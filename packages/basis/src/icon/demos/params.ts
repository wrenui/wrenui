export interface IIconProps {
  // 自定义样式类名
  className?: string;
  // 图标右上角是否显示徽标圆点
  dot?: boolean;
  // 图标右上角显示徽标内容
  badge?: number | string;
  // 徽标自定义内容
  content?: React.ReactNode;
  // 图标颜色
  color?: string;
  // 图标大小(默认单位为px)
  size?: number | string;
}