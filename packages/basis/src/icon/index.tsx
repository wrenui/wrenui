import React, { CSSProperties, FunctionComponent, ReactNode } from 'react';
import './style';
import { Badge } from '../../../../index';
import { isImage, addUnit, isNumeric } from '../../../../common/utils/utils';

interface IProps {
  // 自定义样式类名
  className?: string;
  // 自定义样式
  style?: CSSProperties;
  // 图标名称
  name: string;
  // 图标右上角是否显示徽标圆点
  dot?: boolean;
  // 图标右上角显示徽标内容
  badge?: number | string;
  // 图标右上角显示徽标自定义内容
  content?: ReactNode;
  // 图标颜色
  color?: string;
  // 图标大小(默认单位为px)
  size?: number | string;
  // 点击图标事件
  onClick?: () => void;
}
// 获取url图片大小
const getUrlImgSize = (size?: number | string) => {
  if (size && isNumeric(size)) {
    return { width: `${size}px`, height: `${size}px` };
  }
  if (size) {
    return { width: size, height: size };
  }
  return {};
};
const Icon: FunctionComponent<IProps> = (props) => {
  const { name, dot = false, badge, content, color, size = 32, onClick = () => { } } = props;
  const isImg = isImage(name);
  return (
    <Badge
      tag="i"
      dot={dot}
      digital={badge}
      content={content}
      className={['icon_warpper', `van-icon-${name}`, props.className].filter((v) => v).join(' ')}
      style={{
        ...props.style,
        color,
        fontSize: addUnit(size),
      }}
      onClick={() => { onClick(); }}
    >
      {isImg ? <img className={'icon_img'} src={name} style={getUrlImgSize(size)} /> : <></>}
    </Badge>
  );
};

export default Icon;