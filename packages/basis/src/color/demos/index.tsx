import React, { FunctionComponent } from 'react';
import { Color } from '../../../../../index';
import '../style';

interface IProps {
}

const ColorDemo: FunctionComponent<IProps> = props => {
  return (
    <Color />
  );
};

export default ColorDemo;
