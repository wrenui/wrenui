import React, { FunctionComponent } from 'react';

import color from '../../../../common/assets/color/color.config';
import { copyColor } from '../../../../common/utils/utils';

interface IProps {

}

const Color: FunctionComponent<IProps> = (props) => {
  const toCopyColor = (color: string) => {
    console.log('成功复制到色彩：', copyColor(color));
  };
  return (
    <>
      {color.colorList.map((items: any, itemsIndex: number) => {
        return (
          <div key={items.name} className={'color_box'}>
            <div className={'color_title'}>
              <label>{items.title}</label>
              <span>{items.description}</span>
            </div>
            {
              items.list.map((item: any, itemIndex: number) => {
                return (
                  <div key={item.color} className={'color_item'} style={{ background: item.color }} onClick={() => {toCopyColor(item.color); }}>
                    <label style={{color: itemsIndex === 2 && itemIndex > 3 ? '#657180' : ' #fff'}}>{item.name}</label>
                    <span style={{color: itemsIndex === 2 && itemIndex > 3 ? '#657180' : ' #fff'}}>{item.color}</span>
                  </div>
                );
              })
            }
          </div>
        );
      })}
    </>
  );
};

export default Color;