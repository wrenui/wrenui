import React, { CSSProperties, FunctionComponent } from 'react';
import './style';
import { Icon } from '../../../../index';
import { Loading } from '../../../../index';

// import { router } from 'umi';

interface IProps {
  // 自定义样式类名
  className?: string;
  // 自定义样式内容
  style?: CSSProperties;
  // 组件类型，默认为button标签
  tag?: 'button' | 'a';
  // 按钮类型，默认为default
  type?: 'default' | 'primary' | 'success' | 'warning' | 'error';
  // 原生button按钮type属性
  nativeType?: 'button' | 'submit' | 'reset' | undefined;
  // 按钮文字
  text?: React.ReactNode;
  // 按钮大小
  size?: 'large' | 'normal' | 'small' | 'mini';
  // 是否为简洁按钮
  laconic?: boolean;
  // 是否为细边框按钮
  fineBorder?: boolean;
  // 是否为圆角按钮
  round?: boolean;
  // 是否禁用
  disabled?: boolean;
  // 按钮图标名称或url地址
  icon?: string;
  // 按钮图标大小
  iconSize?: number | string;
  // 按钮图标位置
  iconPosition?: 'left' | 'right';
  // 加载状态
  loading?: boolean;
  // 加载图标颜色
  loadingColor?: string;
  // 加载图标类型
  loadingType?: 'circular' | 'spinner';
  // 加载图标大小
  loadingSize?: string;
  // 加载状态下按钮内容
  loadingContent?: string | null;
  // 加载状态下按钮内容颜色
  loadingContentColor?: string | undefined;
  // 是否为块级按钮
  block?: boolean;
  // 自定义按钮颜色
  color?: string;
  // 跳转url地址
  url?: string;
  // 点击事件
  onClick?: (e: Event) => void;
  // 触摸按钮开始事件
  onTouchStart?: (e: TouchEvent) => void;
}

const Button: FunctionComponent<IProps> = (props) => {
  const {
    className,
    style,
    tag = 'button',
    type = 'default',
    nativeType = 'button',
    text,
    size = 'normal',
    laconic,
    fineBorder,
    round,
    disabled = false,
    icon,
    iconSize = '32px',
    iconPosition = 'left',
    loading = false,
    loadingContent = null,
    loadingContentColor = '#fff',
    loadingColor,
    loadingType = 'circular',
    loadingSize = '20px',
    block,
    color,
    url,
    onClick = () => { },
    onTouchStart = () => { }
  } = props;

  const Tag =  tag || 'button';

  // 点击按钮
  const onClickButton = (event: Event) => {
    if (url) {
      window.location.href = url;
      return;
    }
    onClick(event);
  };

  // 获取加载状态图标
  const renderLoadingIcon = () => {
    return <Loading type={loadingType} color={loadingColor} size={loadingSize} content={loadingContent} contentColor={loadingContentColor} />;
  };

  // 获取按钮图标
  const renderIcon = () => {
    if (loading) {
      return renderLoadingIcon();
    }
    if (icon) {
      return (
        <Icon
          name={icon}
          className={'button_icon'}
          size={iconSize}
        />
      );
    }
    return;
  };

  // 获取按钮样式内容
  const getStyle = () => {
    if (color && !laconic && color.indexOf('gradient') === -1) {
      return { ...style, color: '#fff', backgroundColor: color, borderColor: color } as CSSProperties;
    }
    if (color && !laconic && color.indexOf('gradient') !== -1) {
      return { ...style, color: '#fff', background: color, borderColor: '0' } as CSSProperties;
    }
    if (color && laconic) {
      return { ...style, color, backgroundColor: '#fff', borderColor: color } as CSSProperties;
    }
    return { ...style };
  };

  // 获取按钮内容样式类名
  const getContentClass = () => {
    if (loading || icon) {
      if (!iconPosition || iconPosition === 'left') {
        return loading ? 'button_loading_icon_left' : 'button_content_icon_left';
      }
      if (iconPosition === 'right') {
        return loading ? 'button_loading_icon_right' : 'button_content_icon_right';
      }
    }
    return '';
  };

  // 获取按钮内容
  const renderContent = () => {
    if (!loadingContent && text) {
      return <span className={getContentClass()}>{loadingContent}</span>;
    }
    if (!loadingContent && props.children) {
      return <span className={getContentClass()}>{props.children}</span>;
    }
    return;
  };
  return (
    <Tag
      type={nativeType}
      className={[
        'button_warpper',
        'button_normal',
        `button_${type}`,
        `button_${size}`,
        disabled ? 'button_disabled' : '',
        laconic ? 'button_laconic' : '',
        fineBorder ? 'button_fine_Border' : '',
        round ? 'button_round' : '',
        block ? 'button_block' : '',
        className
      ].filter((v) => v).join(' ')}
      style={getStyle()}
      disabled={disabled}
      onClick={(e: Event) => { onClickButton(e); }}
      onTouchStart={(e: TouchEvent) => { onTouchStart(e); }}
    >
      <div className={'button_content_box'}>
        {iconPosition === 'left' ? renderIcon() : null}
        {renderContent()}
        {iconPosition === 'right' ? renderIcon() : null}
      </div>
    </Tag>
  );
};

export default Button;