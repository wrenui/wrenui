import React, { FunctionComponent } from 'react';
import styles from './demo.module.less';

import { Button } from '../../../../../index';
interface IProps {

}

const ButtonDemo: FunctionComponent<IProps> = (props) => {
  return (
    <>
      <div className={styles.demo_box}>
        <h2>按钮类型</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self}>默认按钮</Button>
          <Button className={styles.button_self} type="primary">主要按钮</Button>
          <Button className={styles.button_self} type="success">成功按钮</Button>
          <Button className={styles.button_self} type="warning">警告按钮</Button>
          <Button className={styles.button_self} type="error">错误按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>简洁按钮</h2>
        <div className={styles.demo_item_box}>
          <Button laconic className={styles.button_self} type="primary">简洁按钮</Button>
          <Button laconic className={styles.button_self} type="success">简洁按钮</Button>
          <Button laconic className={styles.button_self} type="warning">简洁按钮</Button>
          <Button laconic className={styles.button_self} type="error">简洁按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>细边框按钮</h2>
        <div className={styles.demo_item_box}>
          <Button laconic fineBorder className={styles.button_self} type="warning">细边框按钮</Button>
          <Button laconic fineBorder className={styles.button_self} type="success">细边框按钮</Button>
          <Button laconic fineBorder className={styles.button_self} type="primary">细边框按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>禁用按钮</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self} type="primary" disabled>禁用按钮</Button>
          <Button className={styles.button_self} type="warning" disabled>禁用按钮</Button>
          <Button className={styles.button_self} type="error" disabled>禁用按钮</Button>
          <Button className={styles.button_self} type="success" disabled>禁用按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>加载状态</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self} loading loadingColor="#fff" type="primary"></Button>
          <Button className={styles.button_self} loading loadingColor="#fff" loadingContent="加载中..." type="primary" >加载中</Button>
          <Button className={styles.button_self} loading loadingType="spinner" loadingColor="#fff" loadingContent="正在加载" type="success" />
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>按钮形状</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self} type="primary">方形按钮</Button>
          <Button className={styles.button_self} type="success" round>圆角按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>图标按钮</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self} type="primary" icon="plus" iconSize="18px"></Button>
          <Button className={styles.button_self} type="primary" icon="setting-o" iconSize="18px">设置</Button>
          <Button className={styles.button_self} type="success" icon="phone-circle" iconPosition="right" iconSize="22px">接听</Button>
          <Button className={styles.button_self} type="default" icon="https://img.yzcdn.cn/vant/user-active.png" iconSize="22px">url图标按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>按钮尺寸</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self} type="primary" size="large">大按钮</Button>
          <Button className={styles.button_self} type="success" size="normal">常规按钮</Button>
          <Button className={styles.button_self} type="warning" size="small">小按钮</Button>
          <Button className={styles.button_self} type="error" size="mini">迷你按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>块级按钮</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self} type="success" block>块级按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>导航按钮</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self} type="primary" url="http://www.baidu.com">URL跳转按钮</Button>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>自定义按钮颜色</h2>
        <div className={styles.demo_item_box}>
          <Button className={styles.button_self} color="rgb(114, 50, 221)">自定义颜色</Button>
          <Button className={styles.button_self} color="rgb(114, 50, 221)" laconic>自定义颜色</Button>
          <Button className={styles.button_self} color="linear-gradient(136deg, #ff6e4c, #fa4f52)">渐变颜色按钮</Button>
        </div>
      </div>
    </>
  );
};

export default ButtonDemo;