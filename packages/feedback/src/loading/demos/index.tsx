import React, { FunctionComponent } from 'react';
import styles from './demo.module.less';

import { Loading } from '../../../../../index';

interface IProps {

}

const LoadingDemo: FunctionComponent<IProps> = (props) => {
  return (
    <>
      <div className={styles.demo_box}>
        <h2>基础用法</h2>
        <div className={styles.demo_item_box}>
          <Loading className={styles.loading_self} />
          <Loading className={styles.loading_self} type="spinner" />
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>自定义大小</h2>
        <div className={styles.demo_item_box}>
          <Loading className={styles.loading_self} size="24px" />
          <Loading className={styles.loading_self} size="30px" />
          <Loading className={styles.loading_self} size="36px" />
          <Loading className={styles.loading_self} type="spinner" size="24px" />
          <Loading className={styles.loading_self} type="spinner" size="30px" />
          <Loading className={styles.loading_self} type="spinner" size="36px" />
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>自定义颜色</h2>
        <div className={styles.demo_item_box}>
          <Loading className={styles.loading_self} color="#2d8cf0" />
          <Loading className={styles.loading_self} type="spinner" color="#2d8cf0" />
          <Loading className={styles.loading_self} color=" #ff9900" />
          <Loading className={styles.loading_self} type="spinner" color=" #ff9900" />
          <Loading className={styles.loading_self} color="#19be6b" />
          <Loading className={styles.loading_self} type="spinner" color="#19be6b" />
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>加载状态提示文字</h2>
        <div className={styles.demo_item_box}>
          <Loading className={styles.loading_self} content="加载中..." />
          <Loading className={styles.loading_self} type="spinner" content="加载中..." />
        </div>
        <div className={styles.demo_item_box}>
          <Loading className={styles.loading_self}>正在加载...</Loading>
          <Loading className={styles.loading_self} type="spinner" >正在加载...</Loading>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>加载状态图标和提示文字垂直排列</h2>
        <div className={styles.demo_item_box}>
          <Loading className={styles.loading_self} vertical content="加载中..." />
          <Loading className={styles.loading_self} type="spinner" vertical content="加载中..." />
        </div>
      </div>
    </>
  );
};

export default LoadingDemo;