import React, { CSSProperties, FunctionComponent } from 'react';
import './style';
import { addUnit, isNumeric } from '../../../../common/utils/utils';

interface IProps {
  // 自定义样式类名
  className?: string;
  // 自定义样式内容
  style?: CSSProperties;
  // loading类型
  type?: 'circular' | 'spinner';
  // loading颜色
  color?: string;
  // loading图标大小
  size?: number | string;
  // 加载提示文字内容
  content?: React.ReactNode;
  // 加载提示文字颜色
  contentColor?: string | undefined;
  // 加载提示文字大小
  contentSize?: number | string;
  // 是否垂直排列图标文字
  vertical?: boolean;
}

const SpinIcon: React.ReactNode[] = [];
for (let i = 0; i < 12; i++) {
  SpinIcon.push(<i key={`spin_item${i + 1}`} />);
}

const CircularIcon = (
  <svg className={'loading_circular'} viewBox="25 25 50 50">
    <circle cx="50" cy="50" r="20" fill="none" />
  </svg>
);

const Loading: FunctionComponent<IProps> = (props) => {
  const { className = '', style = {}, type = 'circular', color = '#c9c9c9', size = '30px', content, contentColor, contentSize = '14px', vertical } = props;

  // 获取loading图标大小
  const getLoadingStyles = (size?: number | string, color?: string) => {
    let _style = {};
    if (size && isNumeric(size)) {
      _style = { width: `${size}px`, height: `${size}px` };
    }
    if (size) {
      _style = { width: size, height: size };
    }
    if (color) {
      return {..._style, color};
    }
    return _style;
  };

  // 获取loading提示文字
  const renderContent = () => {
    if (content) {
      return <span className={'loading_content'} style={{fontSize: addUnit(contentSize), color: contentColor}}>{content}</span>;
    }
    if (props.children) {
      return <span className={'loading_content'} style={{fontSize: addUnit(contentSize), color: contentColor}}>{props.children}</span>;
    }
    return;
  };

  return (
    <div className={['loading_wrapper', vertical ? 'loading_vertical' : '', className].filter((v) => v).join(' ')} style={style}>
      <span className={['loading_basic', `loading_basic_${type}`].filter((v) => v).join(' ')} style={getLoadingStyles(size, color)}>
        {type === 'circular' ? CircularIcon : SpinIcon}
      </span>
      {renderContent()}
    </div>
  );
};

export default Loading;