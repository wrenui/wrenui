export enum ESheetType {
  basis = 'basis',
  cancel = 'cancel',
  description = 'description',
  status = 'status',
  customize = 'customize',
}