import React, { FunctionComponent, useState } from 'react';
import styles from './demo.module.less';

import { ActionSheet, Cell, CellGroup } from '../../../../../index';
import { ESheetType } from './params';
import { IActions } from '../params';
interface IProps {

}

const basisActions: IActions[] = [{
  name: '选项一'
}, {
  name: '选项二'
}, {
  name: '选项三'
}];
const descriptionActions: IActions[] = [{
  name: '选项一'
}, {
  name: '选项二'
}, {
  name: '选项三',
  subName: '这是二级标题'
}];
const stutsActions: IActions[] = [{
  name: '选项一',
  color: '#2d8cf0'
}, {
  name: '选项二',
  disabled: true
}, {
  name: '选项三',
  loading: true,
  loadingType: 'circular'
}, {
  name: '选项四',
  loading: true,
  loadingType: 'spinner'
}, {
  name: '选项五',
  subName: '这是二级标题'
}];
const ActionSheetDemo: FunctionComponent<IProps> = (props) => {
  // 显示基础用法动作面板
  const [basisSheet, setBasisSheet] = useState<boolean>(false);
  // 显示展示取消按钮动作面板
  const [cancelSheet, setCancelSheet] = useState<boolean>(false);
  // 显示展示描述信息动作面板
  const [descriptionSheet, setDescriptionSheet] = useState<boolean>(false);
  // 显示选项状态动作面板
  const [statusSheet, setStatusSheet] = useState<boolean>(false);
  // 显示自定义内容动作面板
  const [customizeSheet, setCustomizeSheet] = useState<boolean>(false);

  // 点击遮罩层，关闭弹出层
  const onCloseSheet = (popupType: string) => {
    switch (popupType) {
      default:
      case ESheetType.basis:
        setBasisSheet(false);
        break;
      case ESheetType.cancel:
        setCancelSheet(false);
        break;
      case ESheetType.description:
        setDescriptionSheet(false);
        break;
      case ESheetType.status:
        setStatusSheet(false);
        break;
      case ESheetType.customize:
        setCustomizeSheet(false);
        break;
    }
  };
  // 点击选择选项
  const onSelectOptions = (action: IActions, index: number, sheetType: string) => {
    console.log('actions:', action);
    console.log('index:', index);
    onCloseSheet(sheetType);
  };
  return (
    <>
      <CellGroup title="基础用法">
        <Cell arrow onClick={() => { setBasisSheet(true); }}>基础用法</Cell>
        <Cell arrow onClick={() => { setCancelSheet(true); }}>展示取消按钮</Cell>
        <Cell arrow onClick={() => { setDescriptionSheet(true); }}>展示描述信息</Cell>
      </CellGroup>
      <CellGroup title="选项状态">
        <Cell arrow onClick={() => { setStatusSheet(true); }}>选项状态</Cell>
      </CellGroup>
      <CellGroup title="自定义面板">
        <Cell arrow onClick={() => { setCustomizeSheet(true); }}>自定义面板</Cell>
      </CellGroup>
      {/* 基础用法 */}
      <ActionSheet show={basisSheet} zIndex={3000} actions={basisActions}
        onClose={() => { onCloseSheet(ESheetType.basis); }}
        onClickOptions={(action: IActions, index: number) => { onSelectOptions(action, index, ESheetType.basis); }}
      ></ActionSheet>

      {/* 展示取消按钮 */}
      <ActionSheet show={cancelSheet} zIndex={3000} actions={basisActions} cancelText="取消"
        onClose={() => { onCloseSheet(ESheetType.cancel); }}
        onClickOptions={(action: IActions, index: number) => { onSelectOptions(action, index, ESheetType.cancel); }}
      ></ActionSheet>

      {/* 展示描述信息 */}
      <ActionSheet show={descriptionSheet} zIndex={3000} actions={descriptionActions} title="这是标题" description="这是描述信息" cancelText="取消"
        onClose={() => { onCloseSheet(ESheetType.description); }}
        onClickOptions={(action: IActions, index: number) => { onSelectOptions(action, index, ESheetType.description); }}
      ></ActionSheet>

      {/* 展示选项状态 */}
      <ActionSheet show={statusSheet} zIndex={3000} actions={stutsActions} description="这是描述信息" cancelText="关闭"
        onClose={() => { onCloseSheet(ESheetType.status); }}
        onClickOptions={(action: IActions, index: number) => { onSelectOptions(action, index, ESheetType.status); }}
      ></ActionSheet>

      {/* 展示自定义内容 */}
      <ActionSheet show={customizeSheet} zIndex={3000} title="这是标题" closeable={true}
        onClose={() => { onCloseSheet(ESheetType.customize); }}
      >
        <div className={styles.customize_sheet}>
          内容
        </div>
      </ActionSheet>
    </>
  );
};

export default ActionSheetDemo;