import React, { FunctionComponent } from 'react';
import './style';
import { Popup, Loading } from '../../../../index';
import { IActions } from './params';

interface IProps {
  // 是否显示操作面板
  show: boolean;
  // 面板及遮罩层层级
  zIndex?: number | string;
  // 面板选项列表
  actions?: IActions[];
  // 面板标题
  title?: React.ReactNode;
  // 描述信息
  description?: React.ReactNode;
  // 点击遮罩层是否可关闭面板
  closeOnClickOverlay?: boolean;
  // 是否在点击选项后关闭
  closeOnClickAction?: boolean;
  // 是否显示关闭面板按钮
  closeable?: boolean;
  // 取消按钮字符
  cancelText?: React.ReactNode;
  // 弹出层是否圆角
  round?: boolean;
  // 是否显示遮罩层
  overlay?: boolean;
  // 是否锁定背景滚动
  isLockScroll?: boolean;
  // 点击遮罩层或关闭按钮触发关闭事件
  onClose?: () => void;
  // 点击选项事件
  onClickOptions?: (action: IActions, index: number) => void;
}

const ActionSheet: FunctionComponent<IProps> = (props) => {
  const {
    show = false,
    zIndex = 2000,
    actions = [],
    title,
    description,
    closeOnClickOverlay = true,
    closeOnClickAction = true,
    closeable = false,
    cancelText,
    round = true,
    overlay = true,
    isLockScroll = true,
    onClose = () => { },
    onClickOptions = () => { }
  } = props;

  //  获取头部信息/标题
  const renderHeader = () => {
    return (
      <div className={'sheet_header'}>
        {title}
      </div>
    );
  };

  //  获取面板描述信息
  const renderDescription = () => {
    return (
      <div className={'sheet_description'}>
        {description}
      </div>
    );
  };

  // 点击选项
  const toClickOptions = (action: IActions, index: number) => {
    if (action.disabled || action.loading) {
      return;
    }
    if (closeOnClickAction) {
      onClose();
    }
    onClickOptions(action, index);
  };

  //  获取面板操作按钮
  const renderOptions = () => {
    return (
      actions.map((action: IActions, index: number) => {
        return (
          <button
            key={'sheet_item' + index}
            type="button"
            disabled={action.disabled}
            className={['sheet_content_item', action.disabled ? 'sheet_content_item_disabled' : '', action.loading ? 'sheet_content_item_loading' : ''].filter((v) => v).join(' ')}
            onClick={() => { toClickOptions(action, index); }}
          >
            {
              action.loading ? <Loading type={action.loadingType === 'spinner' ? 'spinner' : 'circular'}></Loading> :
                <>
                  <span className={'sheet_content_item_span'} style={{ color: action.color }}>{action.name}</span>
                  {action.subName ? <div className={'sheet_content_item_subname'}>{action.subName}</div> : null}
                </>
            }
          </button>
        );
      })
    );
  };

  //  获取面板取消按钮
  const renderCancel = () => {
    if (!cancelText) {
      return;
    }
    return (
      <>
        <div className={'cancel_top'} />
        <button type="button" className={'cancel_button'} onClick={() => { onClose(); }}>
          {cancelText}
        </button>
      </>
    );
  };

  return (
    <div className={'sheet_wapper'}>
      <Popup
        show={show}
        overlayShow={overlay}
        className={'action_sheet'}
        zIndex={zIndex}
        postion="bottom"
        closeOnClickOverlay={closeOnClickOverlay}
        round={round}
        closeable={closeable}
        isLockScroll={isLockScroll}
        onClose={() => { onClose(); }}
      >
        {/* 头部信息/标题 */}
        {title ? renderHeader() : null}
        {/* 标题/面板描述内容 */}
        {description ? renderDescription() : null}
        {/* 选项内容 */}
        <div className={'sheet_content'}>
          {/* 选项按钮 */}
          {renderOptions()}
          {/* 自定义内容 */}
          {props?.children}
        </div>
        {/* 取消按钮 */}
        {renderCancel()}
      </Popup>
    </div>
  );
};

export default ActionSheet;