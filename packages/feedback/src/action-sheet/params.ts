export interface IActions {
  // 标题
  name: string;
  // 二级标题
  subName?: string;
  // 选项文字字体颜色
  color?: string;
  // 各选项样式类名
  className?: string;
  // 是否为加载中状态
  loading?: boolean;
  // 加载中状态图标类型
  loadingType?: 'circular' | 'spinner';
  // 是否为禁用状态
  disabled?: boolean;
}