import React, { FunctionComponent, useState } from 'react';
import styles from './demo.module.less';
import { Button, Overlay } from '../../../../../index';

interface IProps {

}

const OverlayDemo: FunctionComponent<IProps> = (props) => {
  // 控制基础遮罩层显示
  const [basicDisplay, setBasicDisplay] = useState<boolean>(false);
  // 控制嵌入内容遮罩层显示
  const [contentDisplay, setContentDisplay] = useState<boolean>(false);
  return (
    <>
      <div className={styles.demo_box}>
        <h2>基础用法</h2>
        <div className={styles.demo_item_box}>
          <Button type="primary" onClick={() => { setBasicDisplay(true); }}>显示遮罩层</Button>
        </div>
        <Overlay show={basicDisplay} onClick={() => { setBasicDisplay(false); }} />
      </div>
      <div className={styles.demo_box}>
        <h2>嵌入内容</h2>
        <div className={styles.demo_item_box}>
          <Button type="primary" onClick={() => { setContentDisplay(true); }}>显示嵌入内容遮罩层</Button>
        </div>
        <Overlay show={contentDisplay} onClick={() => { setContentDisplay(false); }}>
          <div className={styles.content}>内容</div>
        </Overlay>
      </div>
    </>
  );
};

export default OverlayDemo;