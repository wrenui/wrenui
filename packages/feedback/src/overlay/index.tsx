import React, { CSSProperties, FunctionComponent } from 'react';
import './style';
import { noop } from '../../../../common/utils/base';
import { CSSTransition } from 'react-transition-group';
interface IProps {
  // 显示控制
  show: boolean;
  // 遮罩层层级
  zIndex?: number | string;
  // 类名
  className?: string;
  // 样式内容
  style?: CSSProperties;
  // 阻止全局滚动
  lockScroll?: boolean;
  // 点击遮罩层
  onClick?: () => void;
}
let preventDefault: any;
const Overlay: FunctionComponent<IProps> = (props) => {
  const { children, show = false, zIndex = 2000, className = '', style = {}, lockScroll = true, onClick = () => { } } = props;
  const styleSelf: CSSProperties = {
    zIndex: zIndex !== undefined ? +zIndex : undefined,
    ...style,
  };
  // 移动端阻止穿透事件
  if (typeof preventDefault === 'undefined') {
    preventDefault = function (e: any) {
      e.preventDefault();
    };
  }
  // 添加监听，阻止滑动
  const addScrollLister = () => {
    document.addEventListener('touchmove', preventDefault, { passive: false });
  };
  // 移除阻止滑动监听
  const removeScrollLister = () => {
    document.removeEventListener('touchmove', preventDefault);
  };
  return (
    <CSSTransition
      in={show}
      classNames="fade"
      timeout={300}
      unmountOnExit
      onEnter={() => { lockScroll ? addScrollLister() : noop(); }}
      onExited={() => { lockScroll ? removeScrollLister() : noop(); }}
      appear={true}>
      <div
        className={['overlay', className].filter((v) => v).join(' ')}
        style={styleSelf}
        onClick={onClick}
      >
        {children}
      </div>
    </CSSTransition>
  );
};

export default Overlay;