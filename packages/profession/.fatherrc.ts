import { IBundleOptions } from 'father';

const options: IBundleOptions = {
  // cjs: 'babel',
  cjs: false,
  esm: 'babel',
  pkgs: [
    'mobile'
  ],
  // 按需加载
  extraBabelPlugins: [
    // antd-mobile
    ['babel-plugin-import', {
      libraryName: 'antd-mobile',
      libraryDirectory: 'es',
      style: true,
    }]
  ],
  cssModules: true
};

export default options;
