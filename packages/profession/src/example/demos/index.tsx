import React, { FunctionComponent } from 'react';
import { Example } from '../../../../../index';
import '../style';

interface IProps {
}

const ExampleDemo: FunctionComponent<IProps> = props => {
  return (
    <Example />
  );
};

export default ExampleDemo;
