import React, { FunctionComponent } from 'react';
import styles from './style/index.less';

interface IProps {
  
}

const Example: FunctionComponent<IProps> = props => {
  return (
    <div className={styles.example_wrapper}>
      这是一个示例
    </div>
  )
}

export default Example;
