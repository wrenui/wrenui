import React, { FunctionComponent } from 'react';
import '../style';
import styles from './demo.module.less';
import { Badge } from '../../../../../index';
interface IProps {

}
const BadgeDemo: FunctionComponent<IProps> = (props) => {
  return (
    <>
      <div className={styles.demo_box}>
        <h2>基础用法</h2>
        <div className={styles.demo_item_box}>
          <Badge className={styles.badge_item} digital="5">
            <div className={styles.badge_child} />
          </Badge>
          <Badge className={styles.badge_item} digital={100}>
            <div className={styles.badge_child} />
          </Badge>
          <Badge className={styles.badge_item} dot>
            <div className={styles.badge_child} />
          </Badge>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>设置最大值</h2>
        <div className={styles.demo_item_box}>
          <Badge className={styles.badge_item} digital="10" max="9">
            <div className={styles.badge_child} />
          </Badge>
          <Badge className={styles.badge_item} digital={100} max={99}>
            <div className={styles.badge_child} />
          </Badge>
          <Badge className={styles.badge_item} digital={1000} max={999}>
            <div className={styles.badge_child} />
          </Badge>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>自定义徽标内容</h2>
        <div className={styles.demo_item_box}>
          <Badge className={styles.badge_item} content={<span>来电</span>} color="#19be6b">
            <div className={styles.badge_child} />
          </Badge>
          <Badge className={styles.badge_item} content={<span>演示</span>} color="#2b85e4">
            <div className={styles.badge_child} />
          </Badge>
          <Badge className={styles.badge_item} content={<span>内容</span>} color="#ff9900">
            <div className={styles.badge_child} />
          </Badge>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>自定义徽标颜色</h2>
        <div className={styles.demo_item_box}>
          <Badge className={styles.badge_item} digital="10" max="9" color="#ff9900">
            <div className={styles.badge_child} />
          </Badge>
          <Badge className={styles.badge_item} digital={100} max={99} color="#19be6b">
            <div className={styles.badge_child} />
          </Badge>
          <Badge className={styles.badge_item} dot color="#2b85e4">
            <div className={styles.badge_child} />
          </Badge>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2>独立仅展示徽标</h2>
        <div className={styles.demo_item_box}>
          <Badge className={styles.badge_item} digital="10" max="9"></Badge>
          <Badge className={styles.badge_item} digital={100} max={99}></Badge>
          <Badge className={styles.badge_item} digital={1000} max={999}></Badge>
        </div>
      </div>
    </>
  );
};

export default BadgeDemo;