import React, { CSSProperties, FunctionComponent, ReactNode } from 'react';
import './style';
import { isDef, isNumeric } from '../../../../common/utils/utils';

interface IProps {
  // 组件根节点HTML标签，默认为div标签
  tag?: 'div' | 'i';
  // 自定义样式类名
  className?: string;
  // 自定义样式
  style?: CSSProperties;
  // 徽标内容(数字)
  digital?: number | string;
  // 徽标自定义内容
  content?: ReactNode;
  // 徽标最大值
  max?: number | string;
  // 默认徽标
  dot?: boolean;
  // 徽标颜色
  color?: string;
  // 点击事件
  onClick?: () => void;
}

const Badge: FunctionComponent<IProps> = (props) => {
  const { tag = 'div', onClick = () => { } } = props;
  const Tag = tag || 'div';
  // 获取徽标内容
  const renderContent = () => {
    const { digital, content, dot, max } = props;
    if (!dot) {
      if (content) {
        return content;
      }
      if (isDef(max) && isNumeric(digital!) && +digital > Number(max)) {
        return `${max}+`;
      }
      return digital;
    }
    return;
  };
  // 获取badge
  const renderBadge = () => {
    if (props.digital || props.content || props.dot) {
      return (
        <div className={['badge_basis', `badge_basis_${tag}`, props.children ? 'badge_fixed' : '', props.dot ? 'badge_dot' : ''].filter((v) => v).join(' ')} style={{ backgroundColor: props.color }}>
          {renderContent()}
        </div>
      );
    }
    return <></>;
  };
  return (
    <Tag className={['badge_wrapper', `badge_${tag}`, props.className].filter((v) => v).join(' ')} style={props.style} onClick={() => { onClick(); }}>
      {props.children ? props.children : <></>}
      {renderBadge()}
    </Tag>
  );
};

export default Badge;