import React, { FunctionComponent } from 'react';
import LazyLoad from 'react-lazy-load';

/**
 * 更多懒加载内容请参考：https://www.npmjs.com/package/react-lazy-load
 */

interface IProps {
  // 待加载内容高
  height: number | string;
  // 待加载内容宽
  width?: number | string;
  // 当该内容加载后回调事件
  onVisible?: (data: React.ReactNode) => void;
}

const lazyLoad: FunctionComponent<IProps> = (props) => {
  const {
    children,
    height,
    width = '100%',
    onVisible = () => { }
  } = props;
  return (
    <LazyLoad height={height} width={width} onContentVisible={() => { onVisible(children); }}>
      {children}
    </LazyLoad>
  );
};

export default lazyLoad;