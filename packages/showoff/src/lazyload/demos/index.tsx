import React, { FunctionComponent } from 'react';
import styles from './demo.module.less';
import { LazyLoad } from '../../../../../index';
import { lazyLoadList } from '../../../../../common/assets/data/index';

interface IProps {

}

const lazyLoadDemo: FunctionComponent<IProps> = (props) => {
  return (
    <>
      {lazyLoadList.map((item: { url: string }, index: number) => {
        return (
          <div key={`lazylad_item${index}`} className={styles.img_box}>
            <span>第{index + 1}张图片</span>
            <LazyLoad key={`img_${index}`} height={220} onVisible={(data: React.ReactNode) => { console.log(data); }}>
              <img className={styles._img} src={item.url}></img>
            </LazyLoad>
          </div>
        );
      })}
    </>
  );
};

export default lazyLoadDemo;