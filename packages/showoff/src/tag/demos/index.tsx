import React, { FunctionComponent, useState } from 'react';
import styles from './demo.module.less';
import { CellGroup, Cell, Tag } from '../../../../../index';

interface IProps {

}

const TagDemo: FunctionComponent<IProps> = (props) => {
  const [closeTag, setCloseTag] = useState<boolean>(true);
  return (
    <>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>基础用法</h2>
        <div className={[styles.demo_item_box, styles.dome_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell title="primary 类型">
              <Tag type="primary">标签</Tag>
            </Cell>
            <Cell title="success 类型">
              <Tag type="success">标签</Tag>
            </Cell>
            <Cell title="warning 类型">
              <Tag type="warning">标签</Tag>
            </Cell>
            <Cell title="error 类型">
              <Tag type="error">标签</Tag>
            </Cell>
          </CellGroup>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>Tag样式风格</h2>
        <div className={[styles.demo_item_box, styles.dome_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell title="开启空心样式">
              <Tag type="success" plain>标签</Tag>
            </Cell>
            <Cell title="开启圆角样式">
              <Tag type="primary" round>标签</Tag>
            </Cell>
            <Cell title="开启标记样式">
              <Tag type="success" mark>标签</Tag>
            </Cell>
            <Cell title="显示关闭按钮">
              <Tag show={closeTag} type="primary" closeable onClose={() => { setCloseTag(false); }}>标签</Tag>
            </Cell>
          </CellGroup>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>Tag标签大小</h2>
        <div className={[styles.demo_item_box, styles.dome_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell title="小号标签">
              <Tag type="warning" size="small">标签</Tag>
            </Cell>
            <Cell title="中号标签">
              <Tag type="warning" size="medium">标签</Tag>
            </Cell>
            <Cell title="大号标签">
              <Tag type="warning" size="large">标签</Tag>
            </Cell>
          </CellGroup>
        </div>
      </div>
      <div className={styles.demo_box}>
        <h2 className={styles.dome_title}>自定义Tag标签颜色</h2>
        <div className={[styles.demo_item_box, styles.dome_item_box_self].filter((v) => v).join(' ')}>
          <CellGroup>
            <Cell title="自定义颜色标签">
              <Tag color="#7232dd">标签</Tag>
            </Cell>
            <Cell title="自定义颜色标签">
              <Tag color="#ffe1e1" textColor="#ad0000">标签</Tag>
            </Cell>
            <Cell title="自定义颜色标签">
              <Tag color="#7232dd" plain>标签</Tag>
            </Cell>
          </CellGroup>
        </div>
      </div>
    </>
  );
};

export default TagDemo;