import React, { CSSProperties, FunctionComponent } from 'react';
import './style';
import { CSSTransition } from 'react-transition-group';
import { Icon } from '../../../../index';

interface IProps {
  // 自定义Tag标签样式类名
  className?: string;
  // 自定义Tag标签样式内容
  style?: CSSProperties;
  // 是否显示tag标签
  show?: boolean;
  // 标签大小
  size?: 'small' | 'medium' | 'large';
  // 标签类型
  type?: 'default' | 'primary' | 'success' | 'warning' | 'error';
  // 是否显示关闭按钮
  closeable?: boolean;
  // 是否开启标记样式
  mark?: boolean;
  // 是否开启空心样式
  plain?: boolean;
  // 是否开启圆角样式
  round?: boolean;
  // 标签自定义颜色
  color?: string;
  // 标签自定义文字颜色
  textColor?: string;
  // 关闭标签事件
  onClose?: () => void;
}

const Tag: FunctionComponent<IProps> = (props) => {
  const {
    className = '',
    style = {},
    type = 'default',
    show = true,
    size = 'small',
    closeable = false,
    mark = false,
    plain = false,
    round = false,
    color,
    textColor,
    onClose = () => { }
  } = props;
  // 获取Tag样式类名
  const getTagClassName = () => {
    const plainClass = plain ? 'tag_plain' : '';
    const roundClass = round ? 'tag_round' : '';
    const markClass = mark ? 'tag_mark' : '';
    const sizeClass = size !== 'small' ? `tag_${size}` : '';
    const _className = ['tag_wrapper', `tag_${type}`, plainClass, roundClass, markClass, sizeClass, className];
    return _className.filter((v) => v).join(' ');
  };
  // 获取Tag样式内容
  const getTagStyle = () => {
    if (plain) {
      return {
        ...style,
        color: textColor || color,
      };
    }
    return {
      ...style,
      color: textColor,
      background: color,
    };
  };
  // 获取关闭按钮
  const closeIcon = () => {
    if (closeable) {
      return <Icon name="cross" className={'tag_close_icon'} size="inherit" onClick={() => { onClose(); }}></Icon>;
    }
    return;
  };
  return (
    <CSSTransition
      in={show}
      classNames={closeable && !show ? 'fade' : undefined}
      timeout={300}
      unmountOnExit
      appear={true}
    >
      <span className={getTagClassName()} style={getTagStyle()}>
        {props.children}
        {closeIcon()}
      </span>
    </CSSTransition>
  );
};

export default Tag;