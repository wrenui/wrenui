import React from 'react';
import version from '../version';

const HomeDemo = () => {
  return (
    <div>
      {Object.keys(version).map((key) => {
        return (
          // @ts-ignore
          <div key={key} style={{marginBottom: '16px', fontSize: '16px'}}>{key}: {version[key]}</div>
        )
      })}
    </div>
  )
};

export default HomeDemo;
